package com.atlassian.sal.core.rdbms;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import javax.annotation.Nonnull;

import com.google.common.annotations.VisibleForTesting;
import io.atlassian.fugue.Option;

import com.atlassian.plugin.util.PluginKeyStack;
import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.api.rdbms.RdbmsException;
import com.atlassian.sal.api.rdbms.TransactionalExecutor;
import com.atlassian.sal.spi.HostConnectionAccessor;
import com.atlassian.util.profiling.Ticker;

import static com.atlassian.util.profiling.Metrics.metric;

/**
 * Default implementation that invokes {@link com.atlassian.sal.spi.HostConnectionAccessor}.
 * <p>
 * Created by {@link com.atlassian.sal.core.rdbms.DefaultTransactionalExecutorFactory}
 *
 * @since 3.0
 */
public class DefaultTransactionalExecutor implements TransactionalExecutor {

    private final HostConnectionAccessor hostConnectionAccessor;
    private static final String TASK_NAME = "taskName";

    @VisibleForTesting
    boolean readOnly;

    @VisibleForTesting
    boolean newTransaction;

    @VisibleForTesting
    String pluginKeyAtCreation;

    public DefaultTransactionalExecutor(
            @Nonnull final HostConnectionAccessor hostConnectionAccessor,
            final boolean readOnly,
            final boolean newTransaction) {
        this.hostConnectionAccessor = hostConnectionAccessor;
        this.readOnly = readOnly;
        this.newTransaction = newTransaction;
        pluginKeyAtCreation = PluginKeyStack.getFirstPluginKey();
    }

    @Override
    /**
     * The invokerPluginKey is the key of the plugin that performs execute
     * When the transactionalExecutor has been copied locally, this will result in the pluginKeyAtCreation
     */
    public <A> A execute(@Nonnull final ConnectionCallback<A> callback) {
        String invokerPluginKey =
                Optional.ofNullable(PluginKeyStack.getFirstPluginKey()).orElse(pluginKeyAtCreation);

        try (Ticker ignored = metric("db.sal.transactionalExecutor")
                .withAnalytics()
                .invokerPluginKey(invokerPluginKey)
                .tag(TASK_NAME, callback.getClass().getName())
                .startLongRunningTimer()) {
            return hostConnectionAccessor.execute(
                    readOnly, newTransaction, connection -> executeInternal(connection, callback));
        }
    }

    @Nonnull
    @Override
    public Option<String> getSchemaName() {
        return hostConnectionAccessor.getSchemaName();
    }

    @Override
    @Nonnull
    public TransactionalExecutor readOnly() {
        readOnly = true;
        return this;
    }

    @Override
    @Nonnull
    public TransactionalExecutor readWrite() {
        readOnly = false;
        return this;
    }

    @Override
    @Nonnull
    public TransactionalExecutor newTransaction() {
        newTransaction = true;
        return this;
    }

    @Override
    @Nonnull
    public TransactionalExecutor existingTransaction() {
        newTransaction = false;
        return this;
    }

    @VisibleForTesting
    <A> A executeInternal(@Nonnull final Connection connection, @Nonnull final ConnectionCallback<A> callback) {
        assertAutoCommitFalse(connection);

        // give the user the restricted connection
        try (final WrappedConnection wrappedConnection = new WrappedConnection(connection)) {
            // execute the user's callback
            return callback.execute(wrappedConnection);
        }
    }

    private void assertAutoCommitFalse(final Connection connection) {
        try {
            if (connection.getAutoCommit()) {
                throw new IllegalStateException(
                        "com.atlassian.sal.spi.HostConnectionAccessor returned connection with autocommit set");
            }
        } catch (final SQLException e) {
            throw new RdbmsException("unable to invoke java.sql.Connection#getAutoCommit", e);
        }
    }
}
