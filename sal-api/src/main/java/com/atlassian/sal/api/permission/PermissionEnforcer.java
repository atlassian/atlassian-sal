package com.atlassian.sal.api.permission;

import com.atlassian.annotations.PublicApi;

/**
 * Allows clients to easily verify that the caller has sufficient permission to access the resource.
 * {@code PermissionEnforcer} takes all permissions into account from the current security context. This includes the
 * calling user's permissions, but also any permission escalations or restrictions that may be in effect (e.g.
 * read-only personal access tokens).
 * <p>
 * The host application ensures that any thrown {@link AuthorisationException} is handled correctly:
 *
 * <h2>Web requests</h2>
 * <ul>
 *     <li>If the user is not authenticated, the user will be redirected to the login page</li>
 *     <li>If the user is authenticated, but lacks the required authorisation, an appropriate error page will be
 *         displayed</li>
 * </ul>
 *
 * <h3>REST requests</h3>
 * <ul>
 *     <li>If the user is not authenticated, a 401 error response is returned</li>
 *     <li>If the user is authenticated, but lacks the required authorisation, a 403 error is returned</li>
 * </ul>
 *
 * @since 3.2
 */
@PublicApi
public interface PermissionEnforcer {

    /**
     * Verifies that the current security context has sufficient authorisation to perform administration tasks. This can
     * either be because the user is an administrator, or because the security context is running with temporarily
     * elevated permissions.
     *
     * @throws AuthorisationException if the current security context lacks the required authorisation
     * @throws NotAuthenticatedException if the current security context lacks the required authorisation <em>and</em>
     *                                   the current user is not authenticated
     */
    void enforceAdmin() throws AuthorisationException;

    /**
     * Verifies that the current user is authenticated.
     *
     * @throws NotAuthenticatedException if the user is not authenticated
     */
    void enforceAuthenticated() throws NotAuthenticatedException;

    /**
     * <p>Verifies that at least ONE of the following criteria is met, otherwise an exception is thrown.</p>
     * <ul>
     *     <li>Is current user UNauthenticated AND is anonymous access enabled for site
     *     (see {@link com.atlassian.sal.api.user.UserManager#isAnonymousAccessEnabled UserManager#isAnonymousAccessEnabled})</li>
     *     <li>Is current user authenticated AND is licensed (see {@link #isLicensed})</li>
     *     <li>Is current user authenticated AND is limited unlicensed access enabled for site
     *     (see {@link com.atlassian.sal.api.user.UserManager#isLimitedUnlicensedAccessEnabled UserManager#isLimitedUnlicensedAccessEnabled})</li>
     * </ul>
     * @throws AuthorisationException if the user does not meet site access criteria
     */
    void enforceSiteAccess() throws AuthorisationException;

    /**
     * Verifies that the current security context has sufficient authorisation to perform system administration tasks.
     * This can either be because the user is a system administrator, or because the security context is running with
     * temporarily elevated permissions.
     *
     * @throws AuthorisationException if the current security context lacks the required authorisation
     * @throws NotAuthenticatedException if the current security context lacks the required authorisation <em>and</em>
     *                                   the current user is not authenticated
     */
    void enforceSystemAdmin() throws AuthorisationException;

    /**
     * Tests whether the current security context has sufficient authorisation to perform administration tasks. This can
     * either be because the user is an administrator, or because the security context is running with temporarily
     * elevated permissions.
     *
     * @return {@code true} if the current security context has sufficient authorisation to perform administration
     *         tasks, otherwise {@code false}
     */
    boolean isAdmin();

    /**
     * @return {@code true} if the current user is authenticated
     */
    boolean isAuthenticated();

    /**
     * <p>Returns whether current user is authenticated AND either:</p>
     * <ul>
     *     <li>Assigned a product license (see {@link #isLicensed})</li>
     *     <li>Site has enabled limited unlicensed access
     *     (see {@link com.atlassian.sal.api.user.UserManager#isLimitedUnlicensedAccessEnabled UserManager#isLimitedUnlicensedAccessEnabled})</li>
     * </ul>
     *
     * @return {@code true} or {@code false}
     */
    boolean isLicensedOrLimitedUnlicensedUser();

    /**
     * <p>Returns whether current user is authenticated and has been assigned one or more product licenses.</p>
     * <p>As Bamboo does not assign user licenses, these users are always considered to be assigned a license.</p>
     *
     * @return {@code true} or {@code false}
     */
    boolean isLicensed();

    /**
     * Tests whether the current security context has sufficient authorisation to perform system administration tasks.
     * This can either be because the user is a system administrator, or because the security context is running with
     * temporarily elevated permissions.
     *
     * @return {@code true} if the current security context has sufficient authorisation to perform system
     *         administration tasks, otherwise {@code false}
     */
    boolean isSystemAdmin();
}
