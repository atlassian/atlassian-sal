package com.atlassian.sal.core.csrf;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.xsrf.XsrfHeaderValidator;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.sal.core.xsrf.XsrfRequestValidatorImpl;

/**
 * Provides an implementation of checking if a request
 * contains either a valid csrf token or a
 * valid csrf header {@link XsrfHeaderValidator#TOKEN_HEADER}.
 *
 * @since v2.10.13
 * @deprecated since v2.10.18 use {@link XsrfRequestValidator} instead.
 */
public class CsrfRequestValidator implements XsrfRequestValidator {
    private final XsrfRequestValidator xsrfRequestValidator;

    public CsrfRequestValidator(XsrfTokenValidator tokenValidator) {
        xsrfRequestValidator = new XsrfRequestValidatorImpl(tokenValidator);
    }

    /**
     * Returns true iff the given request has a valid csrf token or a
     * valid csrf header.
     *
     * @param request the request to check.
     * @return true iff the given request has a valid csrf token or a
     * valid csrf header.
     */
    public boolean validateRequestPassesXsrfChecks(HttpServletRequest request) {
        return xsrfRequestValidator.validateRequestPassesXsrfChecks(request);
    }
}
