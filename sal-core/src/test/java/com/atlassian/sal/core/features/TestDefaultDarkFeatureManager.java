package com.atlassian.sal.core.features;

import java.security.Principal;
import java.util.Collections;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableSet;

import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.features.EnabledDarkFeatures;
import com.atlassian.sal.api.features.InvalidFeatureKeyException;
import com.atlassian.sal.api.features.MissingPermissionException;
import com.atlassian.sal.api.features.SiteDarkFeaturesStorage;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestDefaultDarkFeatureManager {
    private static final String FEATURE_FOO = "foo";
    private static final String INVALID_FEATURE_KEY = "invalid feature key";
    private static final String USER_NAME = "foobar";
    private static final UserKey USER_KEY = new UserKey("foobarKey");

    @Rule
    public final ClearSystemPropertyRule systemPropertyRule = new ClearSystemPropertyRule();

    @Mock
    private UserManager userManager;

    @Mock
    private Principal user;

    @Mock
    private SiteDarkFeaturesStorage siteDarkFeaturesStorage;

    @Mock
    private UserProfile profile;

    @Before
    public void setUp() {
        when(siteDarkFeaturesStorage.getEnabledDarkFeatureSet()).thenReturn(Collections.emptySet());
        when(profile.getUsername()).thenReturn(USER_NAME);
        when(profile.getUserKey()).thenReturn(USER_KEY);
    }

    @Test
    public void systemFeatureEnabledForAllUsersDeprecated() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        assertTrue(createFeatureManager().isFeatureEnabledForAllUsers(FEATURE_FOO));
    }

    @Test
    public void systemFeatureNotEnabledForAllUsersDeprecated() {
        assertNull(System.getProperty(FEATURE_FOO));
        assertFalse(createFeatureManager().isFeatureEnabledForAllUsers(FEATURE_FOO));
    }

    @Test
    public void invalidFeatureKeyIsNeverEnabledForAllUsersDeprecated() {
        assertFalse(createFeatureManager().isFeatureEnabledForAllUsers(INVALID_FEATURE_KEY));
    }

    @Test
    public void systemFeatureEnabledForAnonymousDeprecated() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        final boolean featureEnabledForUser = createFeatureManager().isFeatureEnabledForUser(null, FEATURE_FOO);
        assertThat(featureEnabledForUser, is(true));
    }

    @Test
    public void systemFeatureDisabledForAnonymousDeprecated() {
        final boolean featureEnabledForUser = createFeatureManager().isFeatureEnabledForUser(null, FEATURE_FOO);
        assertThat(featureEnabledForUser, is(false));
    }

    @Test
    public void systemFeatureEnabledForAuthenticatedUserDeprecated() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        givenUserResolved();
        final boolean featureEnabledForUser = createFeatureManager().isFeatureEnabledForUser(USER_KEY, FEATURE_FOO);
        assertThat(featureEnabledForUser, is(true));
    }

    @Test
    public void systemFeatureDisabledForAuthenticatedUserDeprecated() {
        givenUserResolved();
        final boolean featureEnabledForUser = createFeatureManager().isFeatureEnabledForUser(USER_KEY, FEATURE_FOO);
        assertThat(featureEnabledForUser, is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void featureUndefinedForInvalidUserDeprecated() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        givenUserNotResolved();
        createFeatureManager().isFeatureEnabledForUser(USER_KEY, FEATURE_FOO);
    }

    @Test
    public void systemFeatureEnabledForAllUsers() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        assertTrue(createFeatureManager().isEnabledForAllUsers(FEATURE_FOO).get());
    }

    @Test
    public void systemFeatureEnabledThenDisabledForAllUsers() {
        givenUserCanManageFeaturesForAllUsers();
        DarkFeatureManager manager = createFeatureManager();
        manager.enableFeatureForAllUsers(FEATURE_FOO);
        verify(siteDarkFeaturesStorage).enable(FEATURE_FOO);
        when(siteDarkFeaturesStorage.contains(FEATURE_FOO)).thenReturn(true);
        assertTrue(manager.isEnabledForAllUsers(FEATURE_FOO).get());

        manager.disableFeatureForAllUsers(FEATURE_FOO);
        verify(siteDarkFeaturesStorage).disable(FEATURE_FOO);
        when(siteDarkFeaturesStorage.contains(FEATURE_FOO)).thenReturn(false);
        assertFalse(manager.isEnabledForAllUsers(FEATURE_FOO).isPresent());
    }

    @Test
    public void systemFeatureNotEnabledForAllUsers() {
        assertNull(System.getProperty(FEATURE_FOO));
        assertFalse(createFeatureManager().isEnabledForAllUsers(FEATURE_FOO).isPresent());
    }

    @Test
    public void invalidFeatureKeyIsNeverEnabledForAllUsers() {
        assertFalse(
                createFeatureManager().isEnabledForAllUsers(INVALID_FEATURE_KEY).isPresent());
    }

    @Test
    public void systemFeatureEnabledForAnonymous() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        final boolean featureEnabledForUser =
                createFeatureManager().isEnabledForUser(null, FEATURE_FOO).get();
        assertThat(featureEnabledForUser, is(true));
    }

    @Test
    public void systemFeatureDisabledForAnonymous() {
        final boolean featureEnabledForUser =
                createFeatureManager().isEnabledForUser(null, FEATURE_FOO).isPresent();
        assertThat(featureEnabledForUser, is(false));
    }

    @Test
    public void systemFeatureEnabledForAuthenticatedUser() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        givenUserResolved();
        final boolean featureEnabledForUser =
                createFeatureManager().isEnabledForUser(USER_KEY, FEATURE_FOO).get();
        assertThat(featureEnabledForUser, is(true));
    }

    @Test
    public void systemFeatureDisabledForAuthenticatedUser() {
        givenUserResolved();
        final boolean featureEnabledForUser =
                createFeatureManager().isEnabledForUser(USER_KEY, FEATURE_FOO).isPresent();
        assertThat(featureEnabledForUser, is(false));
    }

    @Test(expected = IllegalArgumentException.class)
    public void featureUndefinedForInvalidUser() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        givenUserNotResolved();
        createFeatureManager().isEnabledForUser(USER_KEY, FEATURE_FOO).get();
    }

    @Test
    public void enableFeatureForAllUsersViaSystemProperty() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        final EnabledDarkFeatures enabledDarkFeatures = createFeatureManager().getFeaturesEnabledForAllUsers();
        assertThat(enabledDarkFeatures.getFeatureKeySet(), hasItem(FEATURE_FOO));
    }

    @Test
    public void enableFeatureForAllUsersDuringRuntime() {
        enableDarkFeatureForAllUsersDuringRuntime(FEATURE_FOO);
        final EnabledDarkFeatures enabledDarkFeatures = createFeatureManager().getFeaturesEnabledForAllUsers();
        assertThat(enabledDarkFeatures.getFeatureKeySet(), hasItem(FEATURE_FOO));
    }

    @Test
    public void enabledFeaturesForAnonymous() {
        enableDarkFeatureForAllUsersDuringRuntime(FEATURE_FOO);
        final EnabledDarkFeatures enabledDarkFeatures = createFeatureManager().getFeaturesEnabledForUser(null);
        assertThat(enabledDarkFeatures, is(not(nullValue())));
        assertThat(enabledDarkFeatures.getFeatureKeySet(), hasItem(FEATURE_FOO));
    }

    @Test
    public void enabledFeaturesForAuthenticatedUser() {
        enableDarkFeatureForAllUsersDuringRuntime(FEATURE_FOO);
        givenUserResolved();
        final EnabledDarkFeatures enabledDarkFeatures = createFeatureManager().getFeaturesEnabledForUser(USER_KEY);
        assertThat(enabledDarkFeatures, is(not(nullValue())));
        assertThat(enabledDarkFeatures.getFeatureKeySet(), hasItem(FEATURE_FOO));
    }

    @Test(expected = IllegalArgumentException.class)
    public void enabledFeaturesUndefinedForInvalidUser() {
        givenUserNotResolved();
        createFeatureManager().getFeaturesEnabledForUser(USER_KEY);
    }

    @Test
    public void featureEnabledViaSystemFeatureDeprecated() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        assertTrue(createFeatureManager().isFeatureEnabledForCurrentUser(FEATURE_FOO));
    }

    @Test
    public void featureNotEnabledDeprecated() {
        assertNull(System.getProperty(FEATURE_FOO));
        assertFalse(createFeatureManager().isFeatureEnabledForCurrentUser(FEATURE_FOO));
    }

    @Test
    public void invalidFeatureKeyIsNeverEnabledDeprecated() {
        assertFalse(createFeatureManager().isFeatureEnabledForCurrentUser(INVALID_FEATURE_KEY));
    }

    @Test
    public void featureEnabledViaSystemFeature() {
        enableDarkFeatureViaSystemProperty(FEATURE_FOO);
        assertTrue(createFeatureManager().isEnabledForCurrentUser(FEATURE_FOO).get());
    }

    @Test
    public void featureNotEnabled() {
        assertNull(System.getProperty(FEATURE_FOO));
        assertFalse(createFeatureManager().isEnabledForCurrentUser(FEATURE_FOO).isPresent());
    }

    @Test
    public void invalidFeatureKeyIsNeverEnabled() {
        assertFalse(createFeatureManager()
                .isEnabledForCurrentUser(INVALID_FEATURE_KEY)
                .isPresent());
    }

    @Test
    public void sysadminCanManageFeaturesForAllUsers() {
        givenUserIsAuthenticated();
        givenUserIsSysadmin();
        assertTrue(createFeatureManager().canManageFeaturesForAllUsers());
    }

    @Test
    public void nonSysadminCannotManageFeaturesForAllUsers() {
        givenUserIsAuthenticated();
        givenUserIsNotSysadmin();
        assertFalse(createFeatureManager().canManageFeaturesForAllUsers());
    }

    @Test
    public void anonymousCannotManageFeaturesForAllUsers() {
        givenUserIsNotAuthenticated();
        assertFalse(createFeatureManager().canManageFeaturesForAllUsers());
    }

    @Test(expected = MissingPermissionException.class)
    public void enableFeatureForAllUsersRequiresManagePermission() {
        try {
            givenUserIsNotAuthenticated();
            createFeatureManager().enableFeatureForAllUsers(FEATURE_FOO);
        } finally {
            verifyZeroInteractions(siteDarkFeaturesStorage);
        }
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void enableFeatureForAllUsersRequiresValidFeatureKey() {
        try {
            createFeatureManager().enableFeatureForAllUsers(INVALID_FEATURE_KEY);
        } finally {
            verifyNoMoreInteractions(siteDarkFeaturesStorage);
        }
    }

    @Test
    public void enableFeatureForAllUsers() {
        givenUserCanManageFeaturesForAllUsers();
        createFeatureManager().enableFeatureForAllUsers(FEATURE_FOO);
        verify(siteDarkFeaturesStorage).enable(FEATURE_FOO);
    }

    @Test(expected = MissingPermissionException.class)
    public void disableFeatureForAllUsersRequiresManagePermission() {
        try {
            givenUserIsNotAuthenticated();
            createFeatureManager().disableFeatureForAllUsers(FEATURE_FOO);
        } finally {
            verifyZeroInteractions(siteDarkFeaturesStorage);
        }
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void disableFeatureForAllUsersRequiresValidFeatureKey() {
        try {
            createFeatureManager().disableFeatureForAllUsers(INVALID_FEATURE_KEY);
        } finally {
            verifyNoMoreInteractions(siteDarkFeaturesStorage);
        }
    }

    @Test
    public void disableFeatureForAllUsers() {
        givenUserCanManageFeaturesForAllUsers();
        createFeatureManager().disableFeatureForAllUsers(FEATURE_FOO);
        verify(siteDarkFeaturesStorage).disable(FEATURE_FOO);
    }

    private void givenUserCanManageFeaturesForAllUsers() {
        givenUserIsAuthenticated();
        givenUserIsSysadmin();
    }

    private void givenUserNotResolved() {
        when(userManager.getUserProfile(USER_KEY)).thenReturn(null);
    }

    private void givenUserResolved() {
        when(userManager.getUserProfile(USER_KEY)).thenReturn(profile);
    }

    private void givenUserIsNotAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(null);
    }

    private void givenUserIsAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(USER_KEY);
    }

    private void givenUserIsSysadmin() {
        when(userManager.isSystemAdmin(USER_KEY)).thenReturn(Boolean.TRUE);
    }

    private void givenUserIsNotSysadmin() {
        when(userManager.isSystemAdmin(USER_KEY)).thenReturn(Boolean.FALSE);
    }

    private void enableDarkFeatureViaSystemProperty(final String systemPropertyKey) {
        systemPropertyRule.setPropertyWithDarkFeaturePrefix(systemPropertyKey, "true");
    }

    private void enableDarkFeatureForAllUsersDuringRuntime(final String darkFeatureKey) {
        when(siteDarkFeaturesStorage.getEnabledDarkFeatureSet()).thenReturn(ImmutableSet.of(darkFeatureKey));
    }

    private DefaultDarkFeatureManager createFeatureManager() {
        return new DefaultDarkFeatureManager(userManager, siteDarkFeaturesStorage);
    }
}
