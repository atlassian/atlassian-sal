package com.atlassian.sal.api.features;

import java.util.Map;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.slf4j.Logger;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

import static com.atlassian.sal.api.features.ValidFeatureKeyPredicate.checkFeatureKey;

/**
 * A parameterised plugin module condition for enabling modules in the presence of a dark feature.
 * Pass a param with parameter name "featureKey" containing the dark feature key. Example:
 * <pre>
 *  &lt;web-item key="some-key" section="some/section" weight="1"&gt;
 *      &lt;label key="menu.title"/&gt;
 *      &lt;link&gt;/some/path&lt;/link&gt;
 *      &lt;condition class="com.atlassian.sal.api.features.DarkFeatureEnabledCondition"&gt;
 *          &lt;param name="featureKey"&gt;feature.key&lt;/param&gt;
 *      &lt;/condition&gt;
 *  &lt;/web-item&gt;
 * </pre>
 * The feature key is validated using the {@link ValidFeatureKeyPredicate}.
 *
 * @see ValidFeatureKeyPredicate
 */
@ParametersAreNonnullByDefault
public class DarkFeatureEnabledCondition implements Condition, UrlReadingCondition {
    private static final Logger log = getLogger(DarkFeatureEnabledCondition.class);

    private static final String FEATURE_KEY_INIT_PARAMETER_NAME = "featureKey";

    @VisibleForTesting
    static final boolean DEFAULT_STATE = false; // Off by default seems safer

    private final DarkFeatureManager darkFeatureManager;
    private String featureKey;

    public DarkFeatureEnabledCondition(final DarkFeatureManager darkFeatureManager) {
        this.darkFeatureManager = darkFeatureManager;
    }

    @Override
    public void init(final Map<String, String> params) {
        requireNonNull(params, "params");

        if (params.containsKey(FEATURE_KEY_INIT_PARAMETER_NAME)) {
            featureKey = checkFeatureKey(params.get(FEATURE_KEY_INIT_PARAMETER_NAME));
        } else {
            throw new PluginParseException("Parameter '" + FEATURE_KEY_INIT_PARAMETER_NAME + "' is mandatory.");
        }
    }

    /**
     * For supporting web-resources
     */
    @Override
    public void addToUrl(UrlBuilder urlBuilder) {
        requireNonNull(urlBuilder, "urlBuilder");
        urlBuilder.addToQueryString(featureKey, Boolean.toString(isFeatureEnabled()));
    }

    /**
     * For supporting web-resources
     */
    @Override
    public boolean shouldDisplay(QueryParams queryParams) {
        requireNonNull(queryParams, "queryParams");

        final String queryParamValue = queryParams.get(featureKey);
        if (isBlankOrNull(queryParamValue)) {
            log.trace(
                    "In the web-resource URL, the query param <{}> did not have a value, so defaulted to false",
                    featureKey);
            return DEFAULT_STATE;
        }

        return Boolean.parseBoolean(queryParamValue);
    }

    /**
     * For web fragments support
     */
    @Override
    public boolean shouldDisplay(@Nullable final Map<String, Object> stringObjectMap) {
        return isFeatureEnabled();
    }

    private boolean isFeatureEnabled() {
        try {
            return darkFeatureManager.isEnabledForCurrentUser(featureKey).orElse(DEFAULT_STATE);
        } catch (RuntimeException e) {
            log.trace("Was not able to check if the dark feature <{}> was enabled", featureKey, e);
            return DEFAULT_STATE;
        }
    }

    private static boolean isBlankOrNull(@Nullable String key) {
        return isNull(key)
                ||
                // Replace with #isBlank with Java 11+
                key.trim().isEmpty();
    }
}
