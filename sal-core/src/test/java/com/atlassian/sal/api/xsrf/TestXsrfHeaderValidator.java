package com.atlassian.sal.api.xsrf;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestXsrfHeaderValidator {
    private static final String TOKEN_VALUE = "no-check";
    private XsrfHeaderValidator xsrfHeaderValidator;

    private @Mock HttpServletRequest request;

    @Before
    public void setUp() {
        xsrfHeaderValidator = new XsrfHeaderValidator();
    }

    @Test
    public void testIsValidHeaderValueWithNull() {
        assertFalse(xsrfHeaderValidator.isValidHeaderValue(null));
    }

    @Test
    public void testIsValidHeaderValueWithIncorrectValue() {
        assertFalse(xsrfHeaderValidator.isValidHeaderValue("a"));
    }

    @Test
    public void testIsValidHeaderValueWithCorrectValue() {
        assertTrue(xsrfHeaderValidator.isValidHeaderValue(TOKEN_VALUE));
    }

    @Test
    public void testRequestHasValidXsrfHeaderWithValidValue() {
        when(request.getHeader(XsrfHeaderValidator.TOKEN_HEADER)).thenReturn(TOKEN_VALUE);
        assertTrue(xsrfHeaderValidator.requestHasValidXsrfHeader(request));
    }

    @Test
    public void testRequestHasValidXsrfHeaderWithInvalidValue() {
        when(request.getHeader(XsrfHeaderValidator.TOKEN_HEADER)).thenReturn("a");
        assertFalse(xsrfHeaderValidator.requestHasValidXsrfHeader(request));
    }
}
