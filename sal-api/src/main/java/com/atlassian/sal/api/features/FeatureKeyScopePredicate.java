package com.atlassian.sal.api.features;

import java.util.function.Predicate;
import javax.annotation.Nullable;

/**
 * @since 2.10.1
 */
public class FeatureKeyScopePredicate implements Predicate<FeatureKeyScope> {
    private final FeatureKeyScope featureKeyScope;

    public FeatureKeyScopePredicate(final FeatureKeyScope featureKeyScope) {
        this.featureKeyScope = featureKeyScope;
    }

    public static FeatureKeyScopePredicate filterBy(final FeatureKeyScope featureKeyScope) {
        return new FeatureKeyScopePredicate(featureKeyScope);
    }

    /**
     * @since 4.6.0.
     * @param input the feature key to be compared.
     * @return whether the input matches the predicate
     */
    @Override
    public boolean test(@Nullable final FeatureKeyScope input) {
        return input == featureKeyScope;
    }
}
