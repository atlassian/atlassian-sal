package com.atlassian.sal.api.validate;

import java.util.Collection;
import javax.annotation.Nonnull;

import static java.util.Collections.unmodifiableCollection;

/**
 * The outcome of validating multiple licenses.
 *
 * @since 4.2
 */
public class MultipleLicensesValidationResult {

    private final Collection<LicenseValidationResult> licenseValidationResults;

    public MultipleLicensesValidationResult(
            @Nonnull final Collection<LicenseValidationResult> licenseValidationResults) {
        this.licenseValidationResults = unmodifiableCollection(licenseValidationResults);
    }

    /**
     * @return License validation result of each products
     */
    public Collection<LicenseValidationResult> getLicenseValidationResults() {
        return unmodifiableCollection(licenseValidationResults);
    }

    /**
     * @return true if all the result are valid otherwise false
     */
    public boolean isValid() {
        return licenseValidationResults.stream().allMatch(LicenseValidationResult::isValid);
    }

    /**
     * @return true if at least one result has error otherwise false
     */
    public boolean hasErrors() {
        return licenseValidationResults.stream().anyMatch(res -> res.hasErrors());
    }

    /**
     * @return true if at least one result has warning otherwise false
     */
    public boolean hasWarnings() {
        return licenseValidationResults.stream().anyMatch(res -> res.hasWarnings());
    }
}
