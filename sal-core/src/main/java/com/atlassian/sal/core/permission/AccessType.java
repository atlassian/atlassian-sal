package com.atlassian.sal.core.permission;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.annotations.security.AdminOnly;
import com.atlassian.annotations.security.AnonymousSiteAccess;
import com.atlassian.annotations.security.LicensedOnly;
import com.atlassian.annotations.security.SystemAdminOnly;
import com.atlassian.annotations.security.UnlicensedSiteAccess;
import com.atlassian.annotations.security.UnrestrictedAccess;

/**
 * An enum class to extract security defined access information
 */
public enum AccessType {
    EMPTY(null),
    SYSTEM_ADMIN_ONLY(SystemAdminOnly.class),
    ADMIN_ONLY(AdminOnly.class),
    LICENSED_ONLY(LicensedOnly.class),
    UNLICENSED_SITE_ACCESS(UnlicensedSiteAccess.class),
    ANONYMOUS_SITE_ACCESS(AnonymousSiteAccess.class),
    UNRESTRICTED_ACCESS(UnrestrictedAccess.class);
    private static final Logger log = LoggerFactory.getLogger(AccessType.class);

    private static final Map<String, AccessType> SECURITY_ANNOTATION_TO_ACCESS_TYPE =
            Collections.unmodifiableMap(Arrays.stream(AccessType.values())
                    .filter(accessType -> accessType.annotationType != null)
                    .collect(Collectors.toMap(
                            accessType -> accessType.annotationType.getName(), accessType -> accessType)));

    private final Class<? extends Annotation> annotationType;

    AccessType(final Class<? extends Annotation> annotationType) {
        this.annotationType = annotationType;
    }

    /**
     * {@code additionalMapping} defaults to empty map.
     *
     * @see AccessType#getAccessType(Method, Map)
     */
    public static AccessType getAccessType(Method method) {
        return getAccessType(method, Collections.emptyMap());
    }

    /**
     * {@code additionalMapping} defaults to empty map.
     *
     * @see AccessType#getAccessType(Class, String, Map, Class[])
     */
    public static AccessType getAccessType(
            final Class<?> clazz, final String methodName, final Class<?>... parameterTypes) {
        Objects.requireNonNull(clazz);
        return getAccessTypeInternal(getMethod(clazz, methodName, parameterTypes), clazz, Collections.emptyMap());
    }

    /**
     * {@code method} method is retrieved by reflection.
     *
     * @see AccessType#getAccessType(Method, Map)
     */
    public static AccessType getAccessType(
            final Class<?> clazz,
            final String methodName,
            final Map<String, AccessType> additionalMapping,
            final Class<?>... parameterTypes) {
        Objects.requireNonNull(clazz);
        return getAccessTypeInternal(getMethod(clazz, methodName, parameterTypes), clazz, additionalMapping);
    }

    @Nullable
    private static Method getMethod(Class<?> clazz, String methodName, Class<?>... parameterTypes) {
        Objects.requireNonNull(clazz);
        Method method = null;
        try {
            method = clazz.getDeclaredMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            final String parameterTypesString =
                    Arrays.stream(parameterTypes).map(Class::getName).collect(Collectors.joining(", "));
            log.debug("The input method: " + methodName
                    + " with parameters: " + parameterTypesString
                    + " not found on input class: " + clazz.getName());
        }
        return method;
    }

    /**
     * Calculates the access type of given method based on annotations found on class and method.
     * The method annotation takes precedence class annotation.
     * If multiple security annotations are present an IllegalStateException will be thrown.
     *
     * @param method            A method to gather the annotation from and to get declaring class to check for annotations
     * @param additionalMapping Additional mapping of FQN of annotation to access type it corresponds to,
     *                          used in case the annotation wasn't found in standard set of atlassian annotations.
     * @return The access type calculated for given method based on its class and method.
     */
    public static AccessType getAccessType(final Method method, final Map<String, AccessType> additionalMapping) {
        Objects.requireNonNull(method);
        Objects.requireNonNull(additionalMapping);
        return getAccessTypeInternal(method, method.getDeclaringClass(), additionalMapping);
    }

    private static AccessType getAccessTypeInternal(
            final Method method, Class<?> clazz, final Map<String, AccessType> additionalMapping) {
        Objects.requireNonNull(additionalMapping);
        return Stream.<Supplier<AnnotatedElement>>of(
                        () -> method, () -> validateNoInheritanceAssumed(clazz, additionalMapping))
                .map(Supplier::get)
                .filter(Objects::nonNull)
                .map(annotatedElement -> extractFromAnnotation(annotatedElement, additionalMapping))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst()
                .orElse(EMPTY);
    }

    /**
     * If a clazz has no security annotation and has a parent that has any security annotation
     * we throw an exception to make sure user did not assume that annotation is inherited.
     * @param clazz class to check
     * @param additionalMapping additional map of valid security annotations
     * @return input clazz if an exception is not thrown
     */
    private static AnnotatedElement validateNoInheritanceAssumed(
            Class<?> clazz, Map<String, AccessType> additionalMapping) {
        Class<?> currentClass = clazz;
        if (!firstValid(clazz.getDeclaredAnnotations(), additionalMapping).isPresent()) {
            currentClass = currentClass.getSuperclass();
            while (currentClass != null) {
                Optional<AccessType> accessType = firstValid(currentClass.getDeclaredAnnotations(), additionalMapping);
                if (accessType.isPresent()) {
                    throw new IllegalStateException("Class " + clazz.getName()
                            + ", which has no security annotation, inherits from " + currentClass
                            + ", which has access level set to " + accessType.get()
                            + ". A class that extends an annotated class must be explicitly annotated itself"
                            + " to avoid false assumption about annotation inheritance. "
                            + "Add a security annotation to " + clazz);
                }
                currentClass = currentClass.getSuperclass();
            }
        }

        return clazz;
    }

    private static Optional<AccessType> firstValid(
            Annotation[] annotations, Map<String, AccessType> additionalMapping) {
        return streamOfValid(annotations, additionalMapping).findFirst();
    }

    private static Stream<AccessType> streamOfValid(
            Annotation[] annotations, Map<String, AccessType> additionalMapping) {
        return Arrays.stream(annotations)
                .map(Annotation::annotationType)
                // Annotations come from different class loaders, so we cannot rely on Class#equals.
                // Instead, we compare class's FQN strings.
                .map(Class::getName)
                .map(key -> Optional.ofNullable(SECURITY_ANNOTATION_TO_ACCESS_TYPE.get(key))
                        .orElseGet(() -> additionalMapping.get(key)))
                .filter(Objects::nonNull);
    }

    /**
     * Returns the {@link AccessType} type associated with the annotation attached to the object.
     * <p>
     * Annotations are compared based on name rather than class,
     * because classes may be loaded by different {@link ClassLoader}s.
     * Class loaders are taken into account when comparing classes.
     *
     * @param annotatedElement annotated object to investigate
     * @return {@link AccessType} or {@code null} if there aren't any annotations.
     */
    private static Optional<AccessType> extractFromAnnotation(
            AnnotatedElement annotatedElement, Map<String, AccessType> additionalMapping) {
        Annotation[] annotations = annotatedElement.getDeclaredAnnotations();
        return validateAndReturn(annotations, additionalMapping);
    }

    private static Optional<AccessType> validateAndReturn(
            Annotation[] annotations, Map<String, AccessType> additionalMapping) {
        return streamOfValid(annotations, additionalMapping)
                // reduce executes its operator only if multiple elements are present in the stream
                .reduce((a, b) -> {
                    throw new IllegalStateException("Multiple security annotations found: " + a + " and " + b
                            + ". Only one security annotation is allowed on an element.");
                });
    }
}
