package com.atlassian.sal.core.permission;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.sal.api.permission.AuthorisationException;
import com.atlassian.sal.api.permission.NotAuthenticatedException;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPermissionEnforcerTest {

    private final UserKey userKey = new UserKey("1234");

    @Mock
    private UserManager userManager;

    @InjectMocks
    private DefaultPermissionEnforcer permissionEnforcer;

    @Test
    public void testEnforceAdminWithAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isAdmin(userKey)).thenReturn(true);

        permissionEnforcer.enforceAdmin();
        verify(userManager).isAdmin(userKey);
    }

    @Test
    public void testEnforceAdminThrowsIfNotAdmin() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        AuthorisationException e = assertThrows(AuthorisationException.class, () -> permissionEnforcer.enforceAdmin());
        assertTrue(e.getMessage().contains("must be an administrator"));
    }

    @Test
    public void testEnforceAdminThrowsIfNotAuthenticated() {
        assertThrows(NotAuthenticatedException.class, () -> permissionEnforcer.enforceAdmin());
    }

    @Test
    public void testEnforceAuthenticatedWhenAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        permissionEnforcer.enforceAuthenticated();
        verify(userManager).getRemoteUserKey();
    }

    @Test
    public void testEnforceAuthenticatedThrowsIfNotAuthenticated() {
        assertThrows(NotAuthenticatedException.class, () -> permissionEnforcer.enforceAuthenticated());
    }

    @Test
    public void testEnforceSystemAdminWithSystemAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isSystemAdmin(userKey)).thenReturn(true);

        permissionEnforcer.enforceSystemAdmin();
        verify(userManager).isSystemAdmin(userKey);
    }

    @Test
    public void testEnforceSystemAdminThrowsIfNotSystemAdmin() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        AuthorisationException e =
                assertThrows(AuthorisationException.class, () -> permissionEnforcer.enforceSystemAdmin());
        assertTrue(e.getMessage().contains("must be a system administrator"));
    }

    @Test
    public void testEnforceSystemAdminThrowsIfNotAuthenticated() {
        assertThrows(NotAuthenticatedException.class, () -> permissionEnforcer.enforceSystemAdmin());
    }

    @Test
    public void testIsAdminWithAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isAdmin(userKey)).thenReturn(true);

        assertTrue(permissionEnforcer.isAdmin());
    }

    @Test
    public void testIsAdminIfNotAdmin() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        assertFalse(permissionEnforcer.isAdmin());
    }

    @Test
    public void testIsAdminIfNotAuthenticated() {
        assertFalse(permissionEnforcer.isAdmin());
    }

    @Test
    public void testIsAuthenticatedWhenAuthenticated() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        assertTrue(permissionEnforcer.isAuthenticated());
    }

    @Test
    public void testIsAuthenticatedWhenNotAuthenticated() {
        assertFalse(permissionEnforcer.isAuthenticated());
    }

    @Test
    public void testIsSystemAdminWithSystemAdminPermission() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isSystemAdmin(userKey)).thenReturn(true);

        assertTrue(permissionEnforcer.isSystemAdmin());
    }

    @Test
    public void testIsSystemAdminIfNotAdmin() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);

        assertFalse(permissionEnforcer.isSystemAdmin());
    }

    @Test
    public void testIsSystemAdminIfNotAuthenticated() {
        assertFalse(permissionEnforcer.isSystemAdmin());
    }

    @Test
    public void testIsLicensedAsLicensedUser() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isLicensed(userKey)).thenReturn(true);

        assertTrue(permissionEnforcer.isLicensed());
    }

    @Test
    public void testIsLicensedAsUnlicensedUser() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isLicensed(userKey)).thenReturn(false);

        assertFalse(permissionEnforcer.isLicensed());
    }

    @Test
    public void testIsLicensedOrLimitedUnlicensedUserAsLimitedUnlicensedUser() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isLicensed(userKey)).thenReturn(false);
        when(userManager.isLimitedUnlicensedUser(userKey)).thenReturn(true);

        assertTrue(permissionEnforcer.isLicensedOrLimitedUnlicensedUser());
    }

    @Test
    public void testIsLicensedOrLimitedUnlicensedUserAsNoAccessUser() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isLicensed(userKey)).thenReturn(false);
        when(userManager.isLimitedUnlicensedUser(userKey)).thenReturn(false);

        assertFalse(permissionEnforcer.isLicensedOrLimitedUnlicensedUser());
    }

    @Test
    public void testEnforceSiteAccessAsAnonymousUserWhenAnonymousAccessEnabled() {
        when(userManager.isAnonymousAccessEnabled()).thenReturn(true);

        // assertDoesNotThrow
        permissionEnforcer.enforceSiteAccess();
    }

    @Test
    public void testEnforceSiteAccessAsAnonymousUserWhenAnonymousAccessNotEnabled() {
        when(userManager.isAnonymousAccessEnabled()).thenReturn(false);

        assertThrows(NotAuthenticatedException.class, () -> permissionEnforcer.enforceSiteAccess());
    }

    @Test
    public void testEnforceSiteAccessAsUnlicensedUserWhenAnonymousAccessEnabled() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isLicensed(userKey)).thenReturn(false);
        when(userManager.isLimitedUnlicensedUser(userKey)).thenReturn(false);

        AuthorisationException e =
                assertThrows(AuthorisationException.class, () -> permissionEnforcer.enforceSiteAccess());
        assertTrue(e.getMessage().contains("must have at least limited site access"));
    }

    @Test
    public void testEnforceSiteAccessAsLimitedUnlicensedUser() {
        when(userManager.getRemoteUserKey()).thenReturn(userKey);
        when(userManager.isLicensed(userKey)).thenReturn(false);
        when(userManager.isLimitedUnlicensedUser(userKey)).thenReturn(true);

        // assertDoesNotThrow
        permissionEnforcer.enforceSiteAccess();
    }
}
