package com.atlassian.sal.api.validate;

/**
 * All error types of the license verification's result.
 *
 * @since 4.2
 */
public enum LicenseErrorCode {
    UNKNOWN,

    /**
     * Invalid product key value.
     */
    INVALID_PRODUCT_KEY,

    /**
     * The license is invalid.
     */
    INVALID_LICENSE_KEY,

    /**
     * The license is not for the selected product.
     */
    WRONG_PRODUCT,

    /**
     * The license is expired.
     */
    LICENSE_EXPIRED,

    /**
     * The license's hosting type is incompatible with other product. e.g. Server, Data Center
     */
    INCOMPATIBLE_HOSTING_TYPE,

    /**
     * The license's type is not incompatible.
     */
    INCOMPATIBLE_LICENSE_TYPE,

    /**
     * The license's version is not incompatible.
     */
    INCOMPATIBLE_VERSION,

    /**
     * The license's users/seats are not set or it is equal to zero.
     */
    USER_LIMIT_ISNOT_SET
}
