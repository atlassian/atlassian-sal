package com.atlassian.sal.core.net;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Proxy configuration drawn from standard System properties
 */
public class SystemPropertiesProxyConfig implements ProxyConfig {

    private static final Logger log = LoggerFactory.getLogger(SystemPropertiesProxyConfig.class);

    public static final String PROXY_HOST_PROPERTY_NAME = "http.proxyHost";
    public static final String PROXY_PORT_PROPERTY_NAME = "http.proxyPort";
    public static final String PROXY_USER_PROPERTY_NAME = "http.proxyUser";
    public static final String PROXY_PASSWORD_PROPERTY_NAME = "http.proxyPassword";
    public static final String PROXY_NON_HOSTS_PROPERTY_NAME = "http.nonProxyHosts";

    public static final int DEFAULT_PROXY_PORT = 80;

    private final String proxyHost;
    private final int proxyPort;
    private final String proxyUser;
    private final String proxyPassword;
    private final String[] nonProxyHosts;

    public SystemPropertiesProxyConfig() {
        proxyHost = System.getProperty(PROXY_HOST_PROPERTY_NAME);
        proxyPort = Integer.getInteger(PROXY_PORT_PROPERTY_NAME, DEFAULT_PROXY_PORT);
        proxyUser = System.getProperty(PROXY_USER_PROPERTY_NAME);
        proxyPassword = System.getProperty(PROXY_PASSWORD_PROPERTY_NAME);
        nonProxyHosts = System.getProperty(PROXY_NON_HOSTS_PROPERTY_NAME, "").split("\\|");
        if (log.isDebugEnabled()) {
            log.debug("Found nonProxyHosts - " + Arrays.toString(nonProxyHosts));
        }
    }

    @Override
    public boolean isSet() {
        return StringUtils.isNotBlank(proxyHost);
    }

    @Override
    public boolean requiresAuthentication() {
        return isSet() && StringUtils.isNotBlank(proxyUser);
    }

    @Override
    public String getHost() {
        return proxyHost;
    }

    @Override
    public int getPort() {
        return proxyPort;
    }

    @Override
    public String getUser() {
        return proxyUser;
    }

    @Override
    public String getPassword() {
        return proxyPassword;
    }

    @Override
    public String[] getNonProxyHosts() {
        return nonProxyHosts;
    }

    @Override
    public String toString() {
        return "SystemPropertiesProxyConfig{" + "proxyHost='"
                + proxyHost + '\'' + ", proxyPort="
                + proxyPort + ", proxyUser='"
                + proxyUser + '\'' + ", proxyPassword='"
                + proxyPassword + '\'' + ", nonProxyHosts="
                + Arrays.toString(nonProxyHosts) + '}';
    }
}
