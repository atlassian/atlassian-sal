package com.atlassian.sal.core.auth;

import javax.servlet.ServletRequest;

import com.atlassian.sal.api.auth.OAuthRequestVerifier;
import com.atlassian.sal.api.auth.OAuthRequestVerifierFactory;

public class OAuthRequestVerifierFactoryImpl implements OAuthRequestVerifierFactory {
    private static final OAuthRequestVerifier instance = new OAuthRequestVerifierImpl();

    /**
     * @param request ignored for default ThreadLocal implementation
     */
    public OAuthRequestVerifier getInstance(ServletRequest request) {
        return instance;
    }
}
