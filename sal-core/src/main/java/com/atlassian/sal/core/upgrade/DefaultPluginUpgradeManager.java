package com.atlassian.sal.core.upgrade;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.springframework.beans.factory.InitializingBean;
import org.osgi.framework.BundleReference;
import org.slf4j.Logger;
import com.google.common.collect.ImmutableList;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.upgrade.PluginUpgradeManager;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.sal.core.message.DefaultMessage;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.concurrent.TimeUnit.SECONDS;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.exception.ExceptionUtils.getStackTrace;
import static org.slf4j.LoggerFactory.getLogger;

import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getPluginKey;
import static com.atlassian.sal.core.upgrade.PluginUpgrader.BUILD;

/**
 * Processes plugin upgrade operations.
 * <p>
 * Upgrades are triggered by the start lifecycle event, and plugin enabled.
 */
public class DefaultPluginUpgradeManager implements PluginUpgradeManager, LifecycleAware, InitializingBean {
    private static final Logger log = getLogger(DefaultPluginUpgradeManager.class);

    protected static final String LOCK_TIMEOUT_PROPERTY = "sal.upgrade.task.lock.timeout";
    protected static final int LOCK_TIMEOUT_SECONDS = Integer.getInteger(LOCK_TIMEOUT_PROPERTY, 300000);
    private final String buildSettingsKey;

    private volatile boolean started = false;

    private final List<PluginUpgradeTask> upgradeTasks;
    private final TransactionTemplate transactionTemplate;
    private final PluginAccessor pluginAccessor;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final PluginEventManager pluginEventManager;
    private final ClusterLockService clusterLockService;

    public DefaultPluginUpgradeManager(
            final List<PluginUpgradeTask> upgradeTasks,
            final TransactionTemplate transactionTemplate,
            final PluginAccessor pluginAccessor,
            final PluginSettingsFactory pluginSettingsFactory,
            final PluginEventManager pluginEventManager,
            final ClusterLockService clusterLockService) {
        this(
                upgradeTasks,
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService,
                BUILD);
    }

    public DefaultPluginUpgradeManager(
            final List<PluginUpgradeTask> upgradeTasks,
            final TransactionTemplate transactionTemplate,
            final PluginAccessor pluginAccessor,
            final PluginSettingsFactory pluginSettingsFactory,
            final PluginEventManager pluginEventManager,
            final ClusterLockService clusterLockService,
            final String buildSettingsKey) {
        this.upgradeTasks = upgradeTasks;
        this.transactionTemplate = transactionTemplate;
        this.pluginAccessor = pluginAccessor;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.pluginEventManager = pluginEventManager;
        this.clusterLockService = clusterLockService;
        this.buildSettingsKey = buildSettingsKey;
    }

    /**
     * Notifies the plugin upgrade manager that a plugin update tasks has been registered.
     * <p>
     * This method does nothing but logging at the moment. It is now deprecated since it could result in circular
     * dependency when trying to bind already exposed update tasks to plugin manager that is being created.
     *
     * @param task  the upgrade task that is being bound
     * @param props the set of properties that the upgrade task was registered with
     * @deprecated as of 2.0.16 no longer used we now use the eventing framework to run upgrade tasks for plugins that
     *             are 'enabled', see {@link #onPluginEnabled(com.atlassian.plugin.event.events.PluginEnabledEvent)}
     */
    @Deprecated
    public void onBind(final PluginUpgradeTask task, final Map props) {
        // Doing lots here....
        try {
            if (log.isDebugEnabled()) {
                log.debug("onbind task = [{}, {}]", task.getPluginKey(), task.getBuildNumber());
            }
        } catch (Exception | LinkageError e) {
            log.error("Unable to resolve task build number", e);
        }
    }

    @Override
    public void onStart() {
        log.debug("onStart");
        final List<Message> messages = upgrade();
        messages.forEach(msg -> log.error("Upgrade error: {}", msg));

        started = true;
    }

    @Override
    public void onStop() {
        pluginEventManager.unregister(this);
    }

    @PluginEventListener
    public void onPluginEnabled(PluginEnabledEvent event) {
        // Check if the Application is fully started:
        if (!started) {
            log.info("Ignoring event: {}; LifecycleAware.onStart has not occurred yet", event);
            return;
        }

        // Run upgrades for this plugin that has been enabled AFTER the onStart event.
        final List<Message> messages = upgradeInternal(event.getPlugin());
        if (!messages.isEmpty()) {
            log.error(
                    "Error(s) encountered while upgrading plugin '{}' on event: {}.",
                    event.getPlugin().getName(),
                    event);
            messages.forEach(msg -> log.error("Upgrade error: {}", msg));
        }
    }

    @Override
    public List<Message> upgrade() {
        return upgradeInternal();
    }

    @Override
    public void afterPropertiesSet() {
        pluginEventManager.register(this);
    }

    /**
     * Returns a map of upgrade tasks grouped by plugin key.
     * @return map of all upgrade tasks (stored by pluginKey)
     */
    public Map<String, List<PluginUpgradeTask>> getUpgradeTasks() {
        return getUpgradeTasksInternal(null);
    }

    @Nonnull
    public List<Message> upgradeInternal() {
        log.info("Running plugin upgrade tasks...");
        return upgradeInternal(null);
    }

    @Nonnull
    public List<Message> upgradeInternal(final Plugin plugin) {
        final Map<String, List<PluginUpgradeTask>> pluginUpgrades = getUpgradeTasksInternal(plugin);

        final List<Message> messages = new ArrayList<>();
        for (final String pluginKey : pluginUpgrades.keySet()) {
            if (matches(plugin, pluginKey)) {
                final List<Message> errors = upgradePlugin(pluginKey, pluginUpgrades.get(pluginKey));
                messages.addAll(errors);
            }
        }

        return messages;
    }

    /**
     * Returns a map of upgrade tasks grouped by plugin key.
     * If a plugin is given, then the resulting map will contain only those
     * upgrade tasks that belong to the specified plugin.
     * @return map of all upgrade tasks (stored by pluginKey)
     * @see DefaultPluginUpgradeManager#validate(PluginUpgradeTask)
     */
    @Nonnull
    private Map<String, List<PluginUpgradeTask>> getUpgradeTasksInternal(@Nullable final Plugin plugin) {
        final Map<String, List<PluginUpgradeTask>> pluginUpgrades = new HashMap<>();

        for (final PluginUpgradeTask upgradeTask : upgradeTasks) {
            if (validate(upgradeTask) && matches(plugin, upgradeTask.getPluginKey())) {
                pluginUpgrades
                        .computeIfAbsent(upgradeTask.getPluginKey(), k -> new ArrayList<>())
                        .add(upgradeTask);
            }
        }

        return pluginUpgrades;
    }

    @Nonnull
    private List<Message> upgradePlugin(final String pluginKey, final List<PluginUpgradeTask> upgrades) {
        return transactionTemplate.execute(() -> {
            final Plugin plugin = pluginAccessor.getPlugin(pluginKey);
            if (plugin == null) {
                throw new IllegalArgumentException("Invalid plugin key: " + pluginKey);
            }

            final PluginUpgrader pluginUpgrader = new PluginUpgrader(
                    plugin, pluginSettingsFactory.createGlobalSettings(), buildSettingsKey, upgrades);

            final String lockName = asClusterLockName(pluginKey);
            final ClusterLock lock = clusterLockService.getLockForName(lockName);
            try {
                if (!lock.tryLock(LOCK_TIMEOUT_SECONDS, SECONDS)) {
                    final String timeoutMessage =
                            "unable to acquire cluster lock named '" + lockName + "' after waiting "
                                    + LOCK_TIMEOUT_SECONDS
                                    + " seconds; note that this timeout may be adjusted via the system property '"
                                    + LOCK_TIMEOUT_PROPERTY
                                    + "'";
                    log.error(timeoutMessage);
                    return ImmutableList.of(new DefaultMessage(timeoutMessage));
                }
            } catch (InterruptedException e) {
                final String interruptedMessage =
                        "interrupted while trying to acquire cluster lock named '" + lockName + "' " + e.getMessage();
                log.error(interruptedMessage);
                return ImmutableList.of(new DefaultMessage(interruptedMessage));
            }

            try {
                return pluginUpgrader.upgrade();
            } finally {
                lock.unlock();
            }
        });
    }

    /**
     * Validate plugin upgrade task.
     * <p>
     * All interface methods, except {@code PluginUpgradeTask#doUpgrade}, should
     * satisfy the following conditions:
     * <ul>
     *  <li>no null returns
     *  <li>no unexpected exceptions thrown
     * </ul>
     * If any of the above checks fails, then the upgrade task is skipped altogether.
     * @param upgradeTask   upgrade task to validate
     * @return true, if the given upgrade task is valid, otherwise false.
     */
    private static boolean validate(PluginUpgradeTask upgradeTask) {
        final List<String> methodsToValidate = asList("getBuildNumber", "getShortDescription", "getPluginKey");

        final List<Method> methods = stream(PluginUpgradeTask.class.getMethods())
                .filter(m -> methodsToValidate.contains(m.getName()))
                .collect(toList());

        boolean valid = true;
        for (Method method : methods) {
            String error = "";
            try {
                final Object ret = method.invoke(upgradeTask);
                // Even though PluginUpgradeTask methods are @Nonnull, we need
                // to defend against app developers who don't implement the
                // contract correctly.
                if (ret == null) {
                    error = "returns null";
                    valid = false;
                }
            } catch (final Throwable e) {
                // All methods, except PluginUpgradeTask.doUpgrade, should not throw.
                error = "throws exception " + e + "\n" + getStackTrace(e);
                valid = false;
            }

            if (!error.isEmpty()) {
                log.warn(
                        "Invalid upgrade task: {} ({}); {} {}",
                        upgradeTask.getClass().getName(),
                        getPluginKeySafely(upgradeTask),
                        method.getName(),
                        error);
            }
        }

        return valid;
    }

    /**
     * Checks that given plugin has specified key.
     * @param plugin    plugin to check; if null, then any key matches
     * @param pluginKey plugin key string
     * @return true if plugin has specified key or plugin is null, otherwise false
     */
    private static boolean matches(final Plugin plugin, final String pluginKey) {
        return plugin == null || plugin.getKey().equals(pluginKey);
    }

    /**
     * Obtains plugin name for a given upgrade task without actually invoking
     * the {@code PluginUpgradeTask} interface.
     * <p>
     * It is useful when reporting misbehaving upgrade task. For example,
     * an upgrade task that returns null instead of a valid plugin key.
     *
     * @param upgradeTask upgrade task
     * @return plugin key, if available, otherwise "Unknown app" string
     */
    private static String getPluginKeySafely(final PluginUpgradeTask upgradeTask) {
        final Class<?> upgradeTaskClass = upgradeTask.getClass();

        // Try to find out what bundle the upgrade task is from. Technically we
        // _could_ try to use this as the plugin key, but we don't (shouldn't?).
        // Instead, this is intended to help admins find the offending plugin,
        // so they can disable it until the task can be fixed.
        return (upgradeTaskClass.getClassLoader() instanceof BundleReference)
                ? getPluginKey(((BundleReference) upgradeTaskClass.getClassLoader()).getBundle())
                : "Unknown app";
    }

    /**
     * Makes a cluster lock name based on given plugin key.
     * @param pluginKey plugin key string
     * @return cluster lock name
     */
    private static String asClusterLockName(final String pluginKey) {
        return "sal.upgrade." + pluginKey;
    }
}
