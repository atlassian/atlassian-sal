package com.atlassian.sal.api.net;

/**
 * Interface Request represents a request to retrieve data, using Trusted Apps authentication. To execute a request call
 * {@link Request#execute(com.atlassian.sal.api.net.ResponseHandler)}.
 *
 * This extracts methods originally part of {@link com.atlassian.sal.api.net.Request}
 *
 * @since 3.0.0
 */
public interface TrustedRequest extends Request {
    /**
     * Adds TrustedTokenAuthentication to the request. Trusted token authenticator uses current user to make a trusted
     * application call.
     *
     * @param hostname Hostname that this authentication applies to
     * @return a reference to this object.
     */
    TrustedRequest addTrustedTokenAuthentication(final String hostname);

    /**
     * Adds TrustedTokenAuthentication to the request. Trusted token authenticator uses the passed user to make a
     * trusted application call.
     *
     * @param hostname Hostname that this authentication applies to
     * @param username The user to make the request with
     * @return this
     */
    TrustedRequest addTrustedTokenAuthentication(final String hostname, final String username);
}
