package com.atlassian.sal.core.upgrade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestPluginUpgrader {

    @Mock
    private Plugin mockPlugin;

    @Mock
    private Plugin goodPlugin;

    @Mock
    private Plugin badPlugin;

    @Mock
    private PluginUpgradeTask pluginUpgradeTask;

    @Mock
    private PluginSettings mockPluginSettings;

    public static final String BUILD = ":build";

    @Test
    public void testUpgradeFromScratch() {
        final UpgradeTaskStub task = new UpgradeTaskStub(10);

        PluginInformation info = new PluginInformation();

        when(mockPlugin.getKey()).thenReturn("foo.bar");
        when(mockPlugin.getPluginInformation()).thenReturn(info);

        PluginUpgrader pu =
                new PluginUpgrader(mockPlugin, mockPluginSettings, BUILD, new ArrayList<PluginUpgradeTask>() {
                    {
                        add(task);
                    }
                });

        pu.upgrade();

        assertTrue(task.isUpgraded());
        verify(mockPluginSettings, atLeastOnce()).get(eq("foo.bar:build"));
        verify(mockPluginSettings).put(eq("foo.bar:build"), eq("10"));
        verifyNoMoreInteractions(mockPluginSettings);
    }

    @Test
    public void testUpgradeFrom2To5() {
        final UpgradeTaskStub task1 = new UpgradeTaskStub(1);
        final UpgradeTaskStub task2 = new UpgradeTaskStub(2);
        final UpgradeTaskStub task3 = new UpgradeTaskStub(3);
        final UpgradeTaskStub task4 = new UpgradeTaskStub(4);
        final UpgradeTaskStub task5 = new UpgradeTaskStub(5);

        PluginSettings pluginSettings = new PluginSettings() {
            private Map<String, Object> map = new HashMap<>();

            public Object remove(String key) {
                return map.remove(key);
            }

            public Object put(String key, Object value) {
                return map.put(key, value);
            }

            public Object get(String key) {
                return map.get(key);
            }
        };

        pluginSettings.put("foo.bar:build", "2"); // current data version is 2
        PluginUpgrader pu =
                new PluginUpgrader(mockPlugin, pluginSettings, BUILD, Arrays.asList(new PluginUpgradeTask[] {
                    task1, task5, task3, task2, task4
                })); // intentionally not in right order

        PluginInformation info = new PluginInformation();
        when(mockPlugin.getKey()).thenReturn("foo.bar");
        when(mockPlugin.getPluginInformation()).thenReturn(info);

        pu.upgrade();

        assertFalse(task1.isUpgraded()); // not upgraded because data version was already 2
        assertFalse(task2.isUpgraded()); // not upgraded because data version was already 2
        assertTrue(task3.isUpgraded()); // should upgrade
        assertTrue(task4.isUpgraded()); // should upgrade
        assertTrue(task5.isUpgraded()); // should upgrade

        verify(mockPlugin, atLeastOnce()).getKey();
        verifyNoMoreInteractions(mockPluginSettings);
    }

    @Test
    public void testUpgradeOldVersion() {
        final UpgradeTaskStub task = new UpgradeTaskStub(10);
        PluginInformation info = new PluginInformation();

        when(mockPlugin.getKey()).thenReturn("foo.bar");
        when(mockPlugin.getPluginInformation()).thenReturn(info);
        when(mockPluginSettings.get(eq("foo.bar:build"))).thenReturn("5");

        PluginUpgrader pu =
                new PluginUpgrader(mockPlugin, mockPluginSettings, BUILD, new ArrayList<PluginUpgradeTask>() {
                    {
                        add(task);
                    }
                });

        pu.upgrade();

        assertTrue(task.isUpgraded());
        verify(mockPluginSettings, atLeastOnce()).get(eq("foo.bar:build"));
        verify(mockPluginSettings).put(eq("foo.bar:build"), eq("10"));
        verifyNoMoreInteractions(mockPluginSettings);
    }

    @Test
    public void testUpgradeNoTasks() {
        final UpgradeTaskStub task = new UpgradeTaskStub(10);

        PluginInformation info = new PluginInformation();
        when(mockPlugin.getKey()).thenReturn("foo.bar");
        when(mockPlugin.getPluginInformation()).thenReturn(info);
        when(mockPluginSettings.get(eq("foo.bar:build"))).thenReturn("15");

        PluginUpgrader pu =
                new PluginUpgrader(mockPlugin, mockPluginSettings, BUILD, new ArrayList<PluginUpgradeTask>() {
                    {
                        add(task);
                    }
                });

        pu.upgrade();

        assertFalse(task.isUpgraded());
        verify(mockPluginSettings, atLeastOnce()).get(eq("foo.bar:build"));
        verifyNoMoreInteractions(mockPluginSettings);
    }

    @Test
    public void testCheckBrokenUpgradeTaskFailure() {

        PluginSettings pluginSettings = new PluginSettings() {
            private Map<String, Object> map = new HashMap<>();

            public Object remove(String key) {
                return map.remove(key);
            }

            public Object put(String key, Object value) {
                return map.put(key, value);
            }

            public Object get(String key) {
                return map.get(key);
            }
        };
        // bad plugin - broken upgrade task
        PluginUpgrader pu = new PluginUpgrader(
                badPlugin, pluginSettings, BUILD, Arrays.asList(new PluginUpgradeTask[] {pluginUpgradeTask}));

        PluginInformation info = new PluginInformation();
        when(pluginUpgradeTask.getBuildNumber())
                .thenThrow(
                        NoClassDefFoundError
                                .class); // Throwing (noClassFoundError), When method on the PluginUpgradeTask is
        // called.
        when(badPlugin.getPluginInformation()).thenReturn(info);
        List<Message> errors = pu.upgrade(); // not upgraded (noClassFoundError)
        assertThat(errors, not(empty()));

        // Good plugin - upgrade task succeeded
        pluginUpgradeTask = new UpgradeTaskStub(10);
        pluginSettings.put("foo.bar:build", "6"); // current data version is 6
        pu = new PluginUpgrader(
                goodPlugin, pluginSettings, BUILD, Arrays.asList(new PluginUpgradeTask[] {pluginUpgradeTask}));

        info = new PluginInformation();
        when(goodPlugin.getKey()).thenReturn("foo.bar");
        when(goodPlugin.getPluginInformation()).thenReturn(info);

        errors = pu.upgrade(); // upgrade success
        assertThat(errors, empty());

        verify(goodPlugin, atLeastOnce()).getKey();
        verifyNoMoreInteractions(mockPluginSettings);
    }
}
