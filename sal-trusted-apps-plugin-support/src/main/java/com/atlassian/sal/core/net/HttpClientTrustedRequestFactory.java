package com.atlassian.sal.core.net;

import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;

import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.core.trusted.CertificateFactory;

/**
 * Does NOT support json/xml oject marshalling. Use the atlassian-rest implementation of
 * {@link com.atlassian.sal.api.net.RequestFactory} instead.
 */
public class HttpClientTrustedRequestFactory extends HttpClientRequestFactory {

    private final UserManager userManager;
    private final CertificateFactory certificateFactory;

    public HttpClientTrustedRequestFactory(final UserManager userManager, final CertificateFactory certificateFactory) {
        this.userManager = userManager;
        this.certificateFactory = certificateFactory;
    }

    public HttpClientTrustedRequest createTrustedRequest(final MethodType methodType, final String url) {
        final CloseableHttpClient httpClient = createHttpClient();
        final HttpClientContext clientContext = createClientContext();
        return new HttpClientTrustedRequest(
                userManager, certificateFactory, httpClient, clientContext, methodType, url);
    }
}
