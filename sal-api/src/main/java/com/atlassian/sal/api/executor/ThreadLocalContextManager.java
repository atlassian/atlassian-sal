package com.atlassian.sal.api.executor;

/**
 * Manager for retrieving and storing a single object which represents all thread local state.
 * Objects returned by this interface are opaque to callers and are entirely managed by the host application. These
 * objects can be safely compared using {@link Object#equals(Object)}.
 */
public interface ThreadLocalContextManager<C> {
    /**
     * Get the thread local context of the current thread.
     *
     * @return The thread local context
     */
    C getThreadLocalContext();

    /**
     * Set the thread local context on the current thread.
     *
     * @param context The context to set
     * @throws ClassCastException if the given context is not an instance created via {@link #getThreadLocalContext()}.
     */
    void setThreadLocalContext(C context);

    /**
     * Clear the thread local context on the current thread.
     */
    void clearThreadLocalContext();
}
