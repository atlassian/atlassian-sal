package com.atlassian.sal.api.features;

import java.util.Set;

import com.atlassian.annotations.PublicSpi;

/**
 * Persist site wide dark feature keys. <strong>The storage part used by the default {@link DarkFeatureManager}
 * implementation.</strong>
 *
 * @since 2.10
 */
@PublicSpi
public interface SiteDarkFeaturesStorage {
    /**
     * Tells whether the given dark feature key is defined (exists, is known) site wide.
     *
     * @param featureKey the feature key to be checked; not blank, leading and trailing whitespaces are removed
     * @return <code>true</code> if the site contains the given feature key, <code>false</code> otherwise
     */
    boolean contains(String featureKey);

    /**
     * Enable the given dark feature key site wide.
     *
     * @param featureKey the feature key to be enabled; not blank, leading and trailing whitespaces are removed
     */
    void enable(String featureKey);

    /**
     * Disable the given dark feature key site wide.
     *
     * @param featureKey the feature key to be disabled; not blank, leading and trailing whitespaces are removed
     */
    void disable(String featureKey);

    /**
     * The implementation of this should use an unmodifiable implementation of set as the return type.
     * @since 4.6.0.
     * @return all enabled site wide dark feature keys.
     */
    Set<String> getEnabledDarkFeatureSet();
}
