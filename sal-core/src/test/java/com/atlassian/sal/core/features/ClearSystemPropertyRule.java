package com.atlassian.sal.core.features;

import java.util.HashSet;
import java.util.Set;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * All system properties set through this rule are guaranteed to be cleaned up after the test method has finished.
 */
public class ClearSystemPropertyRule extends TestWatcher {
    private final Set<String> properties = new HashSet<String>();

    /**
     * Set a system property key to the given value. The system property will be cleared when the test finishes.
     *
     * @param key   the system property to be set
     * @param value the value to be associated with the system property
     */
    public void setProperty(final String key, final String value) {
        properties.add(key);
        System.setProperty(key, value);
    }

    /**
     * Set a system property key to the given value. Will always put the {@link DefaultDarkFeatureManager#ATLASSIAN_DARKFEATURE_PREFIX}
     * in for you.
     *
     * @param key   the system property to be set. Will be effectively: {@link DefaultDarkFeatureManager#ATLASSIAN_DARKFEATURE_PREFIX} + key
     * @param value the value to be set
     */
    public void setPropertyWithDarkFeaturePrefix(final String key, final String value) {
        setProperty(DefaultDarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX + key, value);
    }

    @Override
    protected void finished(final Description description) {
        for (final String property : properties) {
            System.clearProperty(property);
        }
    }
}
