package com.atlassian.sal.testresources.pluginsettings;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class MockPluginSettingsFactory implements PluginSettingsFactory {
    private final Map<String, PluginSettings> map = new HashMap<>();

    public PluginSettings createSettingsForKey(String key) {
        PluginSettings pluginSettings = map.get(key);
        if (pluginSettings == null) {
            pluginSettings = new MockPluginSettings();
            map.put(key, pluginSettings);
        }
        return pluginSettings;
    }

    public PluginSettings createGlobalSettings() {
        return createSettingsForKey(null);
    }
}
