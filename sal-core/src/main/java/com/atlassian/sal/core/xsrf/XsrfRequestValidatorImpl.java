package com.atlassian.sal.core.xsrf;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.xsrf.XsrfHeaderValidator;
import com.atlassian.sal.api.xsrf.XsrfRequestValidator;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;

/**
 * Provides an implementation of checking if a request
 * contains either a valid Cross-site request forgery(xsrf) token or a
 * valid xsrf header {@link com.atlassian.sal.api.xsrf.XsrfHeaderValidator#TOKEN_HEADER}.
 *
 * @since v2.10.18
 */
public class XsrfRequestValidatorImpl implements XsrfRequestValidator {
    private static final XsrfHeaderValidator headerValidator = new XsrfHeaderValidator();
    private final XsrfTokenValidator tokenValidator;

    public XsrfRequestValidatorImpl(XsrfTokenValidator tokenValidator) {
        this.tokenValidator = tokenValidator;
    }

    /**
     * Returns true iff the given request has a valid xsrf token or a
     * valid xsrf header.
     *
     * @param request the request to check.
     * @return true iff the given request has a valid xsrf token or a
     * valid xsrf header.
     */
    public boolean validateRequestPassesXsrfChecks(HttpServletRequest request) {
        return headerValidator.requestHasValidXsrfHeader(request) || tokenValidator.validateFormEncodedToken(request);
    }
}
