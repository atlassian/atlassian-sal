package com.atlassian.sal.api.message;

import java.util.Locale;
import java.util.Set;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.user.UserKey;

/**
 * This interface is responsible for resolving the current locale.
 *
 * @since 2.0
 */
public interface LocaleResolver {
    /**
     * Given a request, resolve the {@link Locale} that should be used in internationalization and localization.
     * The locale should be determined by first checking the remote users preferences, then defaulting to the
     * application default locale if no preferred locale is set. May consult the given request for the preferred
     * browser locales (if supported by the product).
     *
     * @param request Request to check
     * @return Locale to be used in i18n and l10n. {@link Locale#getDefault()} if none found.
     */
    Locale getLocale(HttpServletRequest request);

    /**
     * Resolve the Locale that should be used in internationalization and localization.  The
     * locale should be determined by checking the preferences of the user in the current authentication
     * context if available, then default to the application default locale if no preferred locale is set. May consult
     * the current request for the preferred browser locales (if supported by the product).
     *
     * @return Locale to be used in i18n and l10n. {@link Locale#getDefault()} if none found.
     */
    Locale getLocale();

    /**
     * Resolve the Locale that should be used in internationalization and localization.  The locale should be determined
     * by checking the preferences of the given user. If no user is given, or the user cannot be found, or the user does
     * not have any preferred locale, then default to the application default locale. Must not consult the current
     * request for any preferred browser locales.
     *
     * @param userKey the user. Can be null.
     * @return Locale to be used in i18n and l10n. {@link Locale#getDefault()} if none found.
     * @since 2.10
     */
    Locale getLocale(@Nullable UserKey userKey);

    /**
     * Resolve the set Application Locale that should be used in internationalization and localization.
     * The locale returned is the current application locale (also known as a system/site locale) or the default locale if no
     * current application locale is set. Must not consider current user locale. Must not consult the current request for any preferred browser locales.
     *
     * @return Locale to be used in i18n and l10n. {@link Locale#getDefault()} if none found.
     * @since 4.4.0
     */
    Locale getApplicationLocale();

    /**
     * Returns a set of all the supported locales by the host application. This is all the language packs that
     * are installed.
     *
     * @return an unmodifiable set of all the supported locales by the host application. Must contain at least one
     * locale.
     */
    Set<Locale> getSupportedLocales();
}
