package com.atlassian.sal.api.features;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import javax.annotation.concurrent.Immutable;

import com.atlassian.annotations.PublicApi;

import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSet;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

/**
 * Represents a set of enabled features.
 *
 * @since 2.10
 */
@Immutable
@PublicApi
public final class EnabledDarkFeatures {
    /**
     * Shorthand in case there are no enabled dark features, or they are disabled
     */
    public static final EnabledDarkFeatures NONE = new EnabledDarkFeatures(emptyMap());

    private final Map<FeatureKeyScope, Set<String>> enabledFeatures;

    /**
     * Create an instance of enabled features based on the given map.
     *
     * @param enabledFeatures a map of enabled features
     * @since 4.6.0
     */
    public EnabledDarkFeatures(final Map<FeatureKeyScope, Set<String>> enabledFeatures) {
        requireNonNull(enabledFeatures, "enabledFeatures");
        this.enabledFeatures = unmodifiableMap(enabledFeatures);
    }

    /**
     * Return all enabled feature keys, the set will be immutable.
     *
     * @return all enabled feature keys
     * @since 4.6.0
     */
    public Set<String> getFeatureKeySet() {
        return unmodifiableSet(
                enabledFeatures.values().stream().flatMap(Collection::stream).collect(toSet()));
    }

    /**
     * Returns all enabled feature keys matching the given criteria. The set returned will be unmodifiable.
     *
     * @param criteria the filter condition to be applied on the set of enabled features
     * @since 4.6.0
     * @return the filtered set of enabled features
     */
    public Set<String> getFeatureKeys(final Predicate<FeatureKeyScope> criteria) {
        requireNonNull(criteria, "criteria");
        return unmodifiableSet(enabledFeatures.entrySet().stream()
                .filter(entry -> criteria.test(entry.getKey()))
                .map(Map.Entry::getValue)
                .flatMap(Collection::stream)
                .collect(toSet()));
    }

    /**
     * Check that the given feature is enabled
     *
     * @param featureKey the feature key to be checked
     * @return <code>true</code> if the given feature key is enabled, <code>false</code> otherwise
     */
    public boolean isFeatureEnabled(final String featureKey) {
        requireNonNull(featureKey, "featureKey");
        return enabledFeatures.values().stream().anyMatch(set -> set.contains(featureKey));
    }

    @Override
    public String toString() {
        return "EnabledDarkFeatures{enabledFeatures=" + enabledFeatures + '}';
    }
}
