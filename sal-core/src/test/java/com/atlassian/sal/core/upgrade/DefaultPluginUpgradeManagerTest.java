package com.atlassian.sal.core.upgrade;

import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import com.atlassian.sal.core.message.DefaultMessage;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.sal.core.upgrade.DefaultPluginUpgradeManager.LOCK_TIMEOUT_PROPERTY;
import static com.atlassian.sal.core.upgrade.DefaultPluginUpgradeManager.LOCK_TIMEOUT_SECONDS;

@RunWith(MockitoJUnitRunner.class)
public class DefaultPluginUpgradeManagerTest {
    @Mock
    private Plugin plugin;

    @Mock
    private Plugin secondPlugin;

    @Mock
    private PluginUpgradeTask pluginUpgradeTask;

    @Mock
    private PluginUpgradeTask secondPluginUpgradeTask;

    @Mock
    private PluginUpgradeTask thirdPluginUpgradeTask;

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private PluginSettingsFactory pluginSettingsFactory;

    @Mock
    private PluginEventManager pluginEventManager;

    @Mock
    private PluginSettings pluginSettings;

    @Mock
    private ClusterLockService clusterLockService;

    @Mock
    private ClusterLock clusterLock;

    private static final String PLUGIN_KEY = "com.atlassian.plugin.test.upgrade-test";
    private static final String SECOND_PLUGIN_KEY = "com.atlassian.plugin2.test.upgrade-test";
    private static final String BUILD = ":build-foo";
    private static final String CLUSTER_LOCK_NAME = "sal.upgrade." + PLUGIN_KEY;

    private DefaultPluginUpgradeManager pluginUpgradeManager;
    private CountingTransactionTemplate transactionTemplate;

    @Before
    public void setUp() throws Exception {
        transactionTemplate = new CountingTransactionTemplate();

        when(plugin.getKey()).thenReturn(PLUGIN_KEY);
        when(secondPlugin.getKey()).thenReturn(SECOND_PLUGIN_KEY);

        when(pluginAccessor.getPlugin(PLUGIN_KEY)).thenReturn(plugin);
        when(pluginAccessor.getPlugin(SECOND_PLUGIN_KEY)).thenReturn(secondPlugin);

        when(pluginUpgradeTask.getPluginKey()).thenReturn(PLUGIN_KEY);
        when(pluginUpgradeTask.getBuildNumber()).thenReturn(1);
        when(pluginUpgradeTask.getShortDescription()).thenReturn("P1:T1");
        when(pluginUpgradeTask.doUpgrade()).thenReturn(emptyList()); // default is success

        when(secondPluginUpgradeTask.getPluginKey()).thenReturn(PLUGIN_KEY);
        when(secondPluginUpgradeTask.getBuildNumber()).thenReturn(2);
        when(secondPluginUpgradeTask.getShortDescription()).thenReturn("P1:T2");
        when(secondPluginUpgradeTask.doUpgrade()).thenReturn(emptyList()); // default is success

        when(thirdPluginUpgradeTask.getPluginKey()).thenReturn(SECOND_PLUGIN_KEY);
        when(thirdPluginUpgradeTask.getBuildNumber()).thenReturn(1);
        when(thirdPluginUpgradeTask.getShortDescription()).thenReturn("P2:T1");
        when(thirdPluginUpgradeTask.doUpgrade()).thenReturn(emptyList()); // default is success

        when(pluginSettingsFactory.createGlobalSettings()).thenReturn(pluginSettings);

        when(clusterLockService.getLockForName(anyString())).thenReturn(clusterLock);
    }

    @After
    public void tearDown() {
        transactionTemplate.resetTransactionCount();
    }

    @Test
    public void testGetUpgradeTasksIgnoresInvalidTasks() {
        prepareInvalidUpgradeTasks().forEach(this::tryGetUpgradeTasksWithInvalidTasks);
    }

    @Nonnull
    private List<PluginUpgradeTask> prepareInvalidUpgradeTasks() {
        // Invalid PluginUpgradeTask.getBuildNumber - throws an exception.
        final PluginUpgradeTask invalidBuildNumber = mock(PluginUpgradeTask.class);
        when(invalidBuildNumber.getBuildNumber()).thenThrow(NullPointerException.class);
        when(invalidBuildNumber.getShortDescription()).thenReturn("Short description");
        when(invalidBuildNumber.getPluginKey()).thenReturn(PLUGIN_KEY);

        // Invalid PluginUpgradeTask.getShortDescription - returns null.
        final PluginUpgradeTask invalidShortDescription = mock(PluginUpgradeTask.class);
        when(invalidShortDescription.getBuildNumber()).thenReturn(1);
        when(invalidShortDescription.getPluginKey()).thenReturn(PLUGIN_KEY);

        // Invalid PluginUpgradeTask.getPluginKey - returns null.
        final PluginUpgradeTask invalidPluginKey = mock(PluginUpgradeTask.class);
        when(invalidPluginKey.getBuildNumber()).thenReturn(1);
        when(invalidPluginKey.getShortDescription()).thenReturn("Short description");

        return asList(invalidBuildNumber, invalidShortDescription, invalidPluginKey);
    }

    private void tryGetUpgradeTasksWithInvalidTasks(final PluginUpgradeTask invalid) {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                asList(pluginUpgradeTask, secondPluginUpgradeTask, invalid, thirdPluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService);

        final Map<String, List<PluginUpgradeTask>> upgradeTasks = pluginUpgradeManager.getUpgradeTasks();
        assertNotNull("Upgrade tasks should have been returned", upgradeTasks);
        assertFalse("The null key should not be present", upgradeTasks.containsKey(null));
        assertFalse(
                "The invalid upgrade task should not be listed",
                upgradeTasks.values().stream().flatMap(List::stream).anyMatch(task -> invalid == task));
        assertThat(upgradeTasks.get(PLUGIN_KEY), hasSize(2));
        assertThat(upgradeTasks.get(SECOND_PLUGIN_KEY), hasSize(1));
    }

    @Test
    public void testUpgradeInternalIsSuccessful() throws InterruptedException {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                singletonList(pluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService,
                BUILD);

        when(pluginSettings.get(plugin.getKey() + BUILD))
                .thenReturn(Integer.toString(0)); // last upgrade task (data build number)
        when(clusterLock.tryLock(LOCK_TIMEOUT_SECONDS, SECONDS)).thenReturn(true);

        List<Message> messages = pluginUpgradeManager.upgradeInternal(plugin);

        // A successful upgrade task should increment the build data number by a put on plugin settings
        verify(pluginSettings).put(PLUGIN_KEY + BUILD, "1");

        assertThat(messages, empty());

        verify(clusterLock).tryLock(LOCK_TIMEOUT_SECONDS, SECONDS);
        verify(clusterLock).unlock();
    }

    @Test
    public void testUpgradeInternalFailsIfUpgradeFails() throws Exception {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                singletonList(pluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService,
                BUILD);

        when(pluginUpgradeTask.doUpgrade()).thenReturn(singletonList(new DefaultMessage("failed.upgrade")));
        when(clusterLock.tryLock(LOCK_TIMEOUT_SECONDS, SECONDS)).thenReturn(true);

        List<Message> messages = pluginUpgradeManager.upgradeInternal(plugin);

        // A failed upgrade task should not increment the build data number in plugin settings
        verify(pluginSettings, never()).put(anyString(), anyString());

        assertThat(messages, not(empty()));

        ArgumentCaptor<String> lockName = ArgumentCaptor.forClass(String.class);
        verify(clusterLockService).getLockForName(lockName.capture());
        verify(clusterLock).tryLock(LOCK_TIMEOUT_SECONDS, SECONDS);
        verify(clusterLock).unlock();

        pluginUpgradeManager.upgradeInternal(plugin);

        verify(clusterLockService, times(2)).getLockForName(lockName.getValue());
    }

    @Test
    public void testThatOnlyOneTransactionTemplateIsExecutedPerPluginUpgrade() throws InterruptedException {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                asList(pluginUpgradeTask, secondPluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService,
                BUILD);

        when(clusterLock.tryLock(LOCK_TIMEOUT_SECONDS, SECONDS)).thenReturn(true);

        List<Message> messages = pluginUpgradeManager.upgradeInternal(plugin);

        assertThat(messages, empty());
        assertThat(transactionTemplate.getTransactionCount(), is(1));

        // from pluginUpgradeTask, secondPluginUpgradeTask
        verify(pluginSettings).put(PLUGIN_KEY + BUILD, "2");

        verify(clusterLock).tryLock(LOCK_TIMEOUT_SECONDS, SECONDS);
        verify(clusterLock).unlock();
    }

    @Test
    public void testThatOnlyOneTransactionTemplateIsExecutedPerPluginUpgradeWithMultiplePlugins()
            throws InterruptedException {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                asList(pluginUpgradeTask, secondPluginUpgradeTask, thirdPluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService,
                BUILD);

        when(clusterLock.tryLock(LOCK_TIMEOUT_SECONDS, SECONDS)).thenReturn(true);

        List<Message> messages = pluginUpgradeManager.upgrade();

        // from pluginUpgradeTask, secondPluginUpgradeTask
        verify(pluginSettings).put(PLUGIN_KEY + BUILD, "2");
        // thirdPluginUpgradeTask
        verify(pluginSettings).put(SECOND_PLUGIN_KEY + BUILD, "1");

        assertThat(messages, empty());
        assertThat(transactionTemplate.getTransactionCount(), is(2));

        verify(clusterLock, times(2)).tryLock(LOCK_TIMEOUT_SECONDS, SECONDS);
        verify(clusterLock, times(2)).unlock();
    }

    @Test
    public void testUpgradeLockTimeout() throws InterruptedException {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                singletonList(pluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService,
                BUILD);

        when(clusterLock.tryLock(LOCK_TIMEOUT_SECONDS, SECONDS)).thenReturn(false);

        List<Message> messages = pluginUpgradeManager.upgradeInternal(plugin);

        final Message expectedMessage = new DefaultMessage(format(
                "unable to acquire cluster lock named '%s' " + "after waiting %d seconds; note that this timeout "
                        + "may be adjusted via the system property '%s'",
                CLUSTER_LOCK_NAME, LOCK_TIMEOUT_SECONDS, LOCK_TIMEOUT_PROPERTY));
        assertThat(messages, contains(expectedMessage));

        verify(clusterLock).tryLock(LOCK_TIMEOUT_SECONDS, SECONDS);
    }

    @Test
    public void testUpgradeLockInterrupted() throws InterruptedException {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                singletonList(pluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService,
                BUILD);

        final String exceptionMessage = "Exception Message";
        when(clusterLock.tryLock(LOCK_TIMEOUT_SECONDS, SECONDS)).thenThrow(new InterruptedException(exceptionMessage));

        List<Message> messages = pluginUpgradeManager.upgradeInternal(plugin);

        final Message expectedMessage = new DefaultMessage(format(
                "interrupted while trying to acquire cluster lock named '%s' %s", CLUSTER_LOCK_NAME, exceptionMessage));
        assertThat(messages, contains(expectedMessage));

        verify(clusterLock).tryLock(LOCK_TIMEOUT_SECONDS, SECONDS);
    }

    @Test
    public void onStopUnsubscribesTheUpgradeManagerFromThePluginEventManager() {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                singletonList(pluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService);

        pluginUpgradeManager.onStop();

        verify(pluginEventManager).unregister(pluginUpgradeManager);
    }

    @Test
    public void afterPropertiesSetSubscribesTheUpgradeManagerToTheEventManager() throws Exception {
        pluginUpgradeManager = new DefaultPluginUpgradeManager(
                singletonList(pluginUpgradeTask),
                transactionTemplate,
                pluginAccessor,
                pluginSettingsFactory,
                pluginEventManager,
                clusterLockService);

        pluginUpgradeManager.afterPropertiesSet();

        verify(pluginEventManager).register(pluginUpgradeManager);
    }

    private static class CountingTransactionTemplate implements TransactionTemplate {
        private int transactionCount = 0;

        @Override
        public <T> T execute(final TransactionCallback<T> action) {
            transactionCount++;
            return action.doInTransaction();
        }

        void resetTransactionCount() {
            transactionCount = 0;
        }

        int getTransactionCount() {
            return transactionCount;
        }
    }
}
