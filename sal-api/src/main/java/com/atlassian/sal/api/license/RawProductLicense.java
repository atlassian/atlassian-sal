package com.atlassian.sal.api.license;

import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * A raw product license, the product license detail in a string format.
 * @since 4.2
 */
public interface RawProductLicense {

    /**
     * @return The product to add this license to e.g."jira-software", "jira-servicedesk", "jira-core"
     */
    @Nonnull
    Optional<String> getProductKey();

    /**
     * @return The license string
     */
    @Nonnull
    Optional<String> getLicense();

    /**
     *
     * @return The flag to specify that this license will be deleted.
     */
    boolean isDeleteLicense();
}
