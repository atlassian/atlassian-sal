package com.atlassian.sal.api.features;

import java.util.function.Predicate;

import org.junit.Test;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertTrue;

import static com.atlassian.sal.api.features.FeatureKeyScope.ALL_USERS;
import static com.atlassian.sal.api.features.FeatureKeyScope.ALL_USERS_READ_ONLY;
import static com.atlassian.sal.api.features.FeatureKeyScope.CURRENT_USER_ONLY;
import static com.atlassian.sal.api.features.FeatureKeyScopePredicate.filterBy;

public class TestEnabledDarkFeatures {
    private static final String FEATURE_KEY = "feature";
    private static final String ANOTHER_FEATURE_KEY = "another-feature";
    private static final Predicate<FeatureKeyScope> ALL_USERS_READ_ONLY_PREDICATE =
            input -> input == ALL_USERS_READ_ONLY;

    @Test
    public void unmodifiableFeaturesEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(ALL_USERS_READ_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeySet(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(ALL_USERS_READ_ONLY_PREDICATE), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void modifiableFeaturesEnabledForAllUser() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(ALL_USERS);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeySet(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featuresEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(ALL_USERS);
        assertThat(
                enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS_READ_ONLY).or(filterBy(ALL_USERS))),
                containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featuresEnabledForCurrentUser() {
        final EnabledDarkFeatures enabledDarkFeatures = createEnabledDarkFeatures(CURRENT_USER_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeySet(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(CURRENT_USER_ONLY)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featureEnabledForAllAndPerUser() {
        final EnabledDarkFeatures enabledDarkFeatures =
                createEnabledDarkFeatureInTwoScopes(ALL_USERS_READ_ONLY, CURRENT_USER_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeySet(), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS_READ_ONLY)), containsInAnyOrder(FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(CURRENT_USER_ONLY)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void multipleFeaturesEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = createTwoEnabledDarkFeatures(ALL_USERS_READ_ONLY);
        assertTrue(enabledDarkFeatures.isFeatureEnabled(FEATURE_KEY));
        assertTrue(enabledDarkFeatures.isFeatureEnabled(ANOTHER_FEATURE_KEY));
        assertThat(enabledDarkFeatures.getFeatureKeySet(), containsInAnyOrder(FEATURE_KEY, ANOTHER_FEATURE_KEY));
    }

    private EnabledDarkFeatures createEnabledDarkFeatures(final FeatureKeyScope featureKeyScope) {
        return new EnabledDarkFeatures(ImmutableMap.of(featureKeyScope, ImmutableSet.of(FEATURE_KEY)));
    }

    private EnabledDarkFeatures createTwoEnabledDarkFeatures(final FeatureKeyScope featureKeyScope) {
        return new EnabledDarkFeatures(
                ImmutableMap.of(featureKeyScope, ImmutableSet.of(FEATURE_KEY, ANOTHER_FEATURE_KEY)));
    }

    private EnabledDarkFeatures createEnabledDarkFeatureInTwoScopes(
            final FeatureKeyScope featureKeyScope1, final FeatureKeyScope featureKeyScope2) {
        return new EnabledDarkFeatures(ImmutableMap.of(
                featureKeyScope1, ImmutableSet.of(FEATURE_KEY),
                featureKeyScope2, ImmutableSet.of(FEATURE_KEY)));
    }
}
