package com.atlassian.sal.api.features;

import java.util.Collections;
import java.util.Set;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

import static com.atlassian.sal.api.features.FeatureKeyScope.ALL_USERS;
import static com.atlassian.sal.api.features.FeatureKeyScope.ALL_USERS_READ_ONLY;
import static com.atlassian.sal.api.features.FeatureKeyScope.CURRENT_USER_ONLY;
import static com.atlassian.sal.api.features.FeatureKeyScopePredicate.filterBy;

public class TestEnabledDarkFeaturesBuilder {
    private static final String FEATURE_KEY = "feature.key";
    private static final Set<String> ENABLED_FEATURES = Collections.singleton(FEATURE_KEY);

    @Test
    public void unmodifiableFeaturesEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = new EnabledDarkFeaturesBuilder()
                .unmodifiableFeaturesEnabledForAllUsers(ENABLED_FEATURES)
                .build();
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS_READ_ONLY)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void unmodifiableFeaturesEnabledForAllUsersCanHandleNull() {
        final EnabledDarkFeatures enabledDarkFeatures = new EnabledDarkFeaturesBuilder()
                .unmodifiableFeaturesEnabledForAllUsers(null)
                .build();
        assertThat(enabledDarkFeatures.getFeatureKeySet(), is(empty()));
    }

    @Test
    public void featuresEnabledForAllUsers() {
        final EnabledDarkFeatures enabledDarkFeatures = new EnabledDarkFeaturesBuilder()
                .featuresEnabledForAllUsers(ENABLED_FEATURES)
                .build();
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(ALL_USERS)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featuresEnabledForAllUsersCanHandleNull() {
        final EnabledDarkFeatures enabledDarkFeatures = new EnabledDarkFeaturesBuilder()
                .featuresEnabledForAllUsers(null)
                .build();
        assertThat(enabledDarkFeatures.getFeatureKeySet(), is(empty()));
    }

    @Test
    public void featuresEnabledForCurrentUser() {
        final EnabledDarkFeatures enabledDarkFeatures = new EnabledDarkFeaturesBuilder()
                .featuresEnabledForCurrentUser(ENABLED_FEATURES)
                .build();
        assertThat(enabledDarkFeatures.getFeatureKeys(filterBy(CURRENT_USER_ONLY)), containsInAnyOrder(FEATURE_KEY));
    }

    @Test
    public void featuresEnabledForCurrentUserCanHandleNull() {
        final EnabledDarkFeatures enabledDarkFeatures = new EnabledDarkFeaturesBuilder()
                .featuresEnabledForCurrentUser(null)
                .build();
        assertThat(enabledDarkFeatures.getFeatureKeySet(), is(empty()));
    }

    @Test
    public void noFeaturesEnabledAtAll() {
        final EnabledDarkFeatures enabledDarkFeatures = new EnabledDarkFeaturesBuilder().build();
        assertThat(enabledDarkFeatures.getFeatureKeySet(), is(empty()));
    }
}
