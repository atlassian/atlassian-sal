package com.atlassian.sal.core.net;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.http.Header;
import org.apache.http.HttpClientConnection;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ProtocolVersion;
import org.apache.http.conn.ConnectionRequest;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestExecutor;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.core.trusted.CertificateFactory;
import com.atlassian.security.auth.trustedapps.EncryptedCertificate;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestHttpClientTrustedRequest {

    private static final String DUMMY_HOST = "dummy.atlassian.test";
    private static final String DUMMY_HTTP_URL = MessageFormat.format("http://{0}/", DUMMY_HOST);

    private static final String DUMMY_USERNAME = "dummy";
    private static final String DUMMY_TRUSTED_TOKEN_ID = "dummy-id";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private HttpRequestExecutor mockRequestExecutor;

    @Mock
    private HttpClientConnectionManager mockConnectionManager;

    @Mock
    private CertificateFactory certificateFactory;

    @Mock
    private EncryptedCertificate encryptedCertificate;

    @Mock
    private UserManager userManager;

    private HttpClientTrustedRequestFactory requestFactory;

    @Captor
    private ArgumentCaptor<HttpRequest> requestCaptor;

    @Before
    public void setup() throws InterruptedException, ExecutionException, IOException, HttpException {
        requestFactory = new HttpClientWithMockConnectionTrustedRequestFactory(
                userManager, certificateFactory, mockConnectionManager, mockRequestExecutor);

        // Always respond with a 200/OK message
        when(mockRequestExecutor.execute(
                        any(HttpRequest.class), any(HttpClientConnection.class), any(HttpContext.class)))
                .thenReturn(createOkResponse());

        // This allows us to hook in to the connection details that HttpClient would have made
        final HttpClientConnection conn = mock(HttpClientConnection.class);
        final ConnectionRequest connRequest = mock(ConnectionRequest.class);
        when(connRequest.get(anyLong(), any(TimeUnit.class))).thenReturn(conn);
        when(mockConnectionManager.requestConnection(any(HttpRoute.class), any()))
                .thenReturn(connRequest);

        // Setup trusted apps mocks
        when(userManager.getRemoteUsername()).thenReturn(DUMMY_USERNAME);
        when(encryptedCertificate.getID()).thenReturn(DUMMY_TRUSTED_TOKEN_ID);
        when(certificateFactory.createCertificate(eq(DUMMY_USERNAME), eq(DUMMY_HTTP_URL)))
                .thenReturn(encryptedCertificate);
    }

    private static HttpResponse createOkResponse() {
        final BasicHttpResponse response =
                new BasicHttpResponse(new ProtocolVersion("HTTP", 1, 1), HttpStatus.SC_OK, "OK");
        response.setEntity(new StringEntity("test body", StandardCharsets.UTF_8));
        return response;
    }

    // TODO @alex fix TrustedTokenScheme#authenticate
    @Test
    public void assertThatTrustedTokenHeadersAdded() throws ResponseException, IOException, HttpException {
        final HttpClientTrustedRequest request =
                requestFactory.createTrustedRequest(Request.MethodType.GET, DUMMY_HTTP_URL);

        request.addTrustedTokenAuthentication(DUMMY_HOST);
        request.execute();

        verify(mockRequestExecutor)
                .execute(requestCaptor.capture(), any(HttpClientConnection.class), any(HttpContext.class));
        final HttpRequest lastRequest = requestCaptor.getValue();
        final Header[] headers = lastRequest.getHeaders(TrustedApplicationUtils.Header.Request.ID);

        //noinspection unchecked
        assertThat(headers, arrayContaining(headerWithValue(equalTo(DUMMY_TRUSTED_TOKEN_ID))));
    }

    private static Matcher<Header> headerWithValue(final Matcher<String> valueMatcher) {
        return new FeatureMatcher<Header, String>(valueMatcher, "header with value", "header value") {
            @Override
            protected String featureValueOf(final Header header) {
                return header.getValue();
            }
        };
    }
}
