package com.atlassian.sal.testresources.pluginsettings;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.sal.api.pluginsettings.PluginSettings;

public class MockPluginSettings implements PluginSettings {
    private final Map<String, Object> map = new HashMap<>();

    public Object get(String key) {
        return map.get(key);
    }

    public Object put(String key, Object value) {
        return map.put(key, value);
    }

    public Object remove(String key) {
        return map.remove(key);
    }
}
