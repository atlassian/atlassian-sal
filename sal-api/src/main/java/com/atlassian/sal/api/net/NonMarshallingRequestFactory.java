package com.atlassian.sal.api.net;

/**
 * {@code HttpClientRequestFactory} implements this interface.
 * See {@link MarshallingRequestFactory} for the variant that allows marshalling of entities.
 *
 * @since 2.1
 */
public interface NonMarshallingRequestFactory<T extends Request<?, ?>> extends RequestFactory<T> {}
