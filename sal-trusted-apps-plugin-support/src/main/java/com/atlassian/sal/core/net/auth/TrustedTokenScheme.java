package com.atlassian.sal.core.net.auth;

import java.io.Serializable;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.ContextAwareAuthScheme;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.MalformedChallengeException;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.security.auth.trustedapps.EncryptedCertificate;

public class TrustedTokenScheme implements ContextAwareAuthScheme, Serializable {
    private static final Logger log = LoggerFactory.getLogger(TrustedTokenScheme.class);
    private final CertificateCopy certificate;

    public TrustedTokenScheme(final EncryptedCertificate certificate) {
        this.certificate = new CertificateCopy(certificate);
    }

    @Override
    public Header authenticate(final Credentials credentials, final HttpRequest request, final HttpContext context)
            throws AuthenticationException {
        // TODO @alex This is a copy of TrustApplicationUtils.addRequestParameters
        // I would propose instead a TrustApplicationUtils.getRequestHeaders list to use here.
        request.addHeader("X-Seraph-Trusted-App-ID", certificate.getID());
        request.addHeader("X-Seraph-Trusted-App-Cert", certificate.getCertificate());
        request.addHeader("X-Seraph-Trusted-App-Key", certificate.getSecretKey());

        Integer version = certificate.getProtocolVersion();
        if (version != null) {
            request.addHeader("X-Seraph-Trusted-App-Version", version.toString());
        }

        request.addHeader("X-Seraph-Trusted-App-Magic", certificate.getMagicNumber());
        if (certificate.getSignature() != null) {
            request.addHeader("X-Seraph-Trusted-App-Signature", certificate.getSignature());
        }

        /*
          HttpClient just calls request.addHeader() with the value returned here.
          Rather than arbitrarily returning just one of the headers we need, returning null which is safely ignored.
          Have raised https://issues.apache.org/jira/browse/HTTPCLIENT-1607 to change this method to return a HeaderGroup
        */
        return null;
    }

    @Override
    public void processChallenge(final Header header) throws MalformedChallengeException {
        log.warn(
                "Ignoring a call to processChallenge as TrustedTokenScheme is intended for preemptive authentication only.");
    }

    @Override
    public String getSchemeName() {
        return "trustedtoken";
    }

    @Override
    public String getParameter(final String name) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getRealm() {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isConnectionBased() {
        return false;
    }

    @Override
    public boolean isComplete() {
        return true;
    }

    @Override
    public Header authenticate(final Credentials credentials, final HttpRequest request)
            throws AuthenticationException {
        return authenticate(credentials, request, new BasicHttpContext());
    }

    private static final class CertificateCopy implements EncryptedCertificate, Serializable {
        private final String id;
        private final String certificate;
        private final String secretKey;
        private final Integer protocolVersion;
        private final String magicNumber;
        private final String signature;

        public CertificateCopy(final EncryptedCertificate cert) {
            this.id = cert.getID();
            this.certificate = cert.getCertificate();
            this.secretKey = cert.getSecretKey();
            this.protocolVersion = cert.getProtocolVersion();
            this.magicNumber = cert.getMagicNumber();
            this.signature = cert.getSignature();
        }

        @Override
        public String getID() {
            return id;
        }

        @Override
        public String getCertificate() {
            return certificate;
        }

        @Override
        public String getSecretKey() {
            return secretKey;
        }

        @Override
        public Integer getProtocolVersion() {
            return protocolVersion;
        }

        @Override
        public String getMagicNumber() {
            return magicNumber;
        }

        @Override
        public String getSignature() {
            return signature;
        }
    }
}
