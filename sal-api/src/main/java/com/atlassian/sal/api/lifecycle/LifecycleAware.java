package com.atlassian.sal.api.lifecycle;

/**
 * Marker interface that indicates a component wishes to execute some code after application startup.
 * <p>
 * {@link #onStart()} will be invoked:
 * <ol>
 * <li>immediately after the host application has started up; and</li>
 * <li>immediately after the host application has been restored from backup (if the host supports backup and restore); and</li>
 * <li>immediately after a plugin module is enabled, if the plugin is installed or enabled manually after the host application
 * has already started.</li>
 * </ol>
 * <p>
 * {@link #onStop()} will be invoked only if {@link #onStart()} was invoked, when dependencies are still available:
 * <ol>
 * <li>on plugin framework shut down</li>
 * <li>on plugin disabling</li>
 * </ol>
 * <p>
 * <strong>Note:</strong> for this to work a component must be exposed as an OSGi service with {@code LifecycleAware} as a declared
 * interface. For transformerless plugins using the atlassian spring scanner, this can be done using the {@code ExportAsService}
 * annotation. For transformed plugins, use a {@code <component>} with a {@code public="true"} attribute and an
 * {@code <interface>com.atlassian.sal.api.lifecycle.LifecycleAware</interface>} child element. For example:
 * <pre>
 *      &lt;component key="my-cool-component" class="com.atlassian.stash.MyCoolComponent" public="true"&gt;
 *          &lt;interface&gt;com.atlassian.sal.api.lifecycle.LifecycleAware&lt;/interface&gt;
 *      &lt;/component&gt;
 * </pre>
 *
 * @since 2.0
 */
public interface LifecycleAware {
    /**
     * Notification that the plugin is enabled and the application is started, including after being restored from backup.
     */
    default void onStart() {}

    /**
     * Notification that the plugin is going to be stopped.
     * <p>
     * Will be invoked if previously notified with {@link #onStart()}, before the plugin is disabled or framework is
     * shut down.
     *
     * @since 3.0
     */
    default void onStop() {}
}
