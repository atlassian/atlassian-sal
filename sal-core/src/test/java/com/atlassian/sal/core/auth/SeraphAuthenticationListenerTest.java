package com.atlassian.sal.core.auth;

import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Test;

import com.atlassian.sal.api.auth.Authenticator;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.atlassian.seraph.filter.BaseLoginFilter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SeraphAuthenticationListenerTest {

    @Test
    public void whenSuccessfullyAuthenticatedRequestAndSessionAttributesAreUpdated() {
        final String ALREADY_FILTERED = "loginfilter.already.filtered";
        final SeraphAuthenticationListener listener = new SeraphAuthenticationListener();
        final Authenticator.Result result = mock(Authenticator.Result.class);
        final HttpServletRequest request = mock(HttpServletRequest.class);
        final HttpServletResponse response = mock(HttpServletResponse.class);
        final HttpSession session = mock(HttpSession.class);
        final Principal principal = mock(Principal.class);

        when(request.getSession()).thenReturn(session);
        when(result.getPrincipal()).thenReturn(principal);

        listener.authenticationSuccess(result, request, response);

        verify(session).setAttribute(DefaultAuthenticator.LOGGED_IN_KEY, principal);
        verify(session).setAttribute(DefaultAuthenticator.LOGGED_OUT_KEY, null);
        verify(request).setAttribute(BaseLoginFilter.OS_AUTHSTATUS_KEY, BaseLoginFilter.LOGIN_SUCCESS);
        verify(request).setAttribute(ALREADY_FILTERED, Boolean.TRUE);
    }
}
