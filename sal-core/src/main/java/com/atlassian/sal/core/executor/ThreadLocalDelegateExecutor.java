package com.atlassian.sal.core.executor;

import java.util.concurrent.Executor;

import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;

import static java.util.Objects.requireNonNull;

/**
 * Executor that wraps executing runnables in a wrapper that transfers the threadlocal context
 */
class ThreadLocalDelegateExecutor implements Executor {
    private final Executor delegate;
    private final ThreadLocalDelegateExecutorFactory delegateExecutorFactory;

    ThreadLocalDelegateExecutor(Executor delegate, ThreadLocalDelegateExecutorFactory delegateExecutorFactory) {
        this.delegateExecutorFactory = requireNonNull(delegateExecutorFactory);
        this.delegate = requireNonNull(delegate);
    }

    @Override
    public void execute(Runnable runnable) {
        final Runnable wrapper = delegateExecutorFactory.createRunnable(runnable);
        delegate.execute(wrapper);
    }
}
