package com.atlassian.sal.core.net;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProxyUtil {
    private static final Logger log = LoggerFactory.getLogger(ProxyUtil.class);

    /**
     * Check if proxy configuration requires authentication before sending request to requestUrl
     * @param proxyConfig proxy configuration
     * @param requestUrl url
     * @return true if proxy authentication should be requested before fetching requestUrl
     */
    static boolean requiresAuthentication(final ProxyConfig proxyConfig, final String requestUrl) {
        if (proxyConfig.getNonProxyHosts().length != 0 && proxyConfig.requiresAuthentication()) {
            try {
                final String host = new URI(requestUrl).getHost();
                if (!shouldBeProxied(host, proxyConfig.getNonProxyHosts())) {
                    return false;
                }
            } catch (URISyntaxException e) {
                log.debug("Can't get host value from {}", requestUrl);
            }
        }
        return proxyConfig.requiresAuthentication();
    }

    /**
     * Check if host should be proxied.
     * @param host to be checked
     * @param nonProxyHosts array of nonProxyHosts, wildcards supported
     * @return true if proxy server should be used to access host
     */
    static boolean shouldBeProxied(@Nullable String host, @Nonnull String[] nonProxyHosts) {
        if (StringUtils.isBlank(host)) {
            return false;
        }
        try {
            for (String nonProxyHost : nonProxyHosts) {
                String pattern = nonProxyHost
                        .replace(".", "\\.")
                        .replace("*", ".*")
                        .replace("[", "\\[")
                        .replace("]", "\\]");
                if (host.matches(pattern)) {
                    return false;
                }
            }
        } catch (Exception e) {
            log.debug(
                    "Failed to match host {} against non proxy hosts {}, will assume host should be proxied: {}",
                    host,
                    Arrays.toString(nonProxyHosts),
                    e.getMessage());
        }
        return true;
    }
}
