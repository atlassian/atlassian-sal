package com.atlassian.sal.core.net;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.impl.conn.DefaultRoutePlanner;
import org.apache.http.protocol.HttpContext;

public class ProxyRoutePlanner extends DefaultRoutePlanner {
    private final HttpHost proxy;
    private final String[] nonProxyHosts;

    public ProxyRoutePlanner(final ProxyConfig proxyConfig) {
        super(null);
        this.proxy = new HttpHost(proxyConfig.getHost(), proxyConfig.getPort());
        this.nonProxyHosts = proxyConfig.getNonProxyHosts();
    }

    @Override
    protected HttpHost determineProxy(final HttpHost target, final HttpRequest request, final HttpContext context)
            throws HttpException {
        return ProxyUtil.shouldBeProxied(target.getHostName(), nonProxyHosts) ? proxy : null;
    }
}
