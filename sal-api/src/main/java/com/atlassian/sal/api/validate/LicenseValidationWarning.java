package com.atlassian.sal.api.validate;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * The license validation warning details.
 *
 * @since 4.2
 */
public class LicenseValidationWarning {

    private final LicenseWarningCode licenseWarningCode;

    private final String warningMessage;

    public LicenseValidationWarning(
            @Nonnull final LicenseWarningCode licenseWarningCode, @Nonnull final String warningMessage) {
        this.licenseWarningCode = requireNonNull(licenseWarningCode);
        this.warningMessage = requireNonNull(warningMessage);
    }

    /**
     * @return The warning type.
     */
    @Nonnull
    public LicenseWarningCode getLicenseWarningCode() {
        return licenseWarningCode;
    }

    /**
     * These should normally be localised to the end user's locale.
     *
     * @return Warning message to explain what is wrong.
     */
    @Nonnull
    public String getWarningMessage() {
        return warningMessage;
    }
}
