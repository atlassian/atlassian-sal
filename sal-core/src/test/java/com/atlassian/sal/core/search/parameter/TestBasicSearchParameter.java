package com.atlassian.sal.core.search.parameter;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(value = Parameterized.class)
public class TestBasicSearchParameter {

    private String queryKey;
    private String queryValue;
    private String expectedQueryString;

    public TestBasicSearchParameter(final String queryKey, final String queryValue, final String expectedQueryString) {
        this.queryKey = queryKey;
        this.queryValue = queryValue;
        this.expectedQueryString = expectedQueryString;
    }

    @Parameters
    public static Iterable<Object[]> testData() {
        /*
            According to RFC 3986 (http://www.ietf.org/rfc/rfc3986.txt), the ABNF for a query is:

                query = *( pchar / "/" / "?" )

            Where:

                pchar = unreserved / pct-encoded / sub-delims / ":" / "@"

            And:

                unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
                pct-encoded   = "%" HEXDIG HEXDIG
                sub-delims  = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="

            Thus all characters other than unreserved or /, ?, :, @ must be encoded, as we can't know whether unencoded
            sub-delims characters would conflict with the broader query parameter segement of a URL.
        */
        return Arrays.asList(new Object[][] {
            {"fixfor", "3.12", "fixfor=3.12"},
            {"fix?for", "3.12!%", "fix%3Ffor=3.12%21%25"},
            {"key!@#$%^&*()", "value!@#$%^&*()", "key%21%40%23%24%25%5E%26*%28%29=value%21%40%23%24%25%5E%26*%28%29"},
            {"key&name", "value&name=", "key%26name=value%26name%3D"},
            {"key", "value with spaces", "key=value+with+spaces"},
            {"key", "value+with+pluses", "key=value%2Bwith%2Bpluses"},
        });
    }

    @Test
    public void assertThatQueryParametersAreCorrectlyEncodedAndDecoded() {
        BasicSearchParameter basicSearchParameter = new BasicSearchParameter(queryKey, queryValue);
        String queryString = basicSearchParameter.buildQueryString();
        assertThat(queryString, is(equalTo(expectedQueryString)));

        BasicSearchParameter searchParameter = new BasicSearchParameter(queryString);
        assertThat(searchParameter.getName(), is(equalTo(queryKey)));
        assertThat(searchParameter.getValue(), is(equalTo(queryValue)));
    }
}
