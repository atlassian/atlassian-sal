package com.atlassian.sal.api.validate;

import java.util.Collection;
import java.util.Optional;
import javax.annotation.Nonnull;

import com.google.common.collect.ImmutableSet;

import com.atlassian.annotations.PublicApi;

import static java.util.Collections.*;
import static java.util.Objects.requireNonNull;

/**
 * The outcome of a license validation check for each product key.
 *
 * @since 4.2
 */
@PublicApi
public class LicenseValidationResult {

    private final Optional<String> productKey;

    private final Optional<String> license;

    private final ImmutableSet<LicenseValidationError> errorMessages;

    private final ImmutableSet<LicenseValidationWarning> warningMessages;

    private LicenseValidationResult(
            @Nonnull final Optional<String> productKey,
            @Nonnull final Optional<String> license,
            @Nonnull final Iterable<LicenseValidationError> errorMessages,
            @Nonnull final Iterable<LicenseValidationWarning> warningMessages) {
        this.productKey = requireNonNull(productKey);
        this.license = requireNonNull(license);
        this.errorMessages = ImmutableSet.copyOf(requireNonNull(errorMessages));
        this.warningMessages = ImmutableSet.copyOf(requireNonNull(warningMessages));
    }

    public static LicenseValidationResult withErrorMessages(
            @Nonnull final Optional<String> productKey,
            @Nonnull final Optional<String> license,
            @Nonnull final Iterable<LicenseValidationError> errorMessages) {
        return new LicenseValidationResult(productKey, license, errorMessages, emptySet());
    }

    public static LicenseValidationResult withWarningMessages(
            @Nonnull final Optional<String> productKey,
            @Nonnull final Optional<String> license,
            @Nonnull final Iterable<LicenseValidationWarning> warningMessages) {
        return new LicenseValidationResult(productKey, license, emptySet(), warningMessages);
    }

    public static LicenseValidationResult withErrorAndWarningMessages(
            @Nonnull final Optional<String> productKey,
            @Nonnull final Optional<String> license,
            @Nonnull final Iterable<LicenseValidationError> errorMessages,
            @Nonnull final Iterable<LicenseValidationWarning> warningMessages) {
        return new LicenseValidationResult(productKey, license, errorMessages, warningMessages);
    }

    public static LicenseValidationResult withValidResult(
            @Nonnull final Optional<String> productKey, @Nonnull final Optional<String> license) {
        return new LicenseValidationResult(productKey, license, emptySet(), emptySet());
    }

    /**
     * @return product key of this class
     */
    public Optional<String> getProductKey() {
        return productKey;
    }

    /**
     * @return the raw license String that gets validated. It could be valid or invalid depends on the {@link #isValid()}
     */
    public Optional<String> getLicense() {
        return license;
    }

    /**
     * Return true if the validation passed, false if there were errors. (Note warnings do not cause this method to
     * return false).
     *
     * @return true if the validation passed, false if there were errors.
     * @see #hasWarnings()
     */
    public boolean isValid() {
        return errorMessages.isEmpty();
    }

    /**
     * Return true if the validation added error messages.
     *
     * @return true if the validation failed with error messages.
     */
    public boolean hasErrors() {
        return !errorMessages.isEmpty();
    }

    /**
     * Return true if the validation added warning messages.
     *
     * @return true if the validation failed with warning messages.
     */
    public boolean hasWarnings() {
        return !warningMessages.isEmpty();
    }

    /**
     * Returns validation error for each products.
     * <p>
     * This will never be null, but may be empty.
     *
     * @return validation error messages, could be empty but not null.
     */
    @Nonnull
    public Collection<LicenseValidationError> getErrorMessages() {
        return unmodifiableCollection(errorMessages);
    }

    /**
     * Returns validation warnings for each products.
     * <p>
     * This will never be null, but may be empty.
     *
     * @return validation warning messages, could be empty but not null.
     */
    @Nonnull
    public Collection<LicenseValidationWarning> getWarningMessages() {
        return unmodifiableCollection(warningMessages);
    }
}
