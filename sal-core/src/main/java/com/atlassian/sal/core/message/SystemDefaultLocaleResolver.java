package com.atlassian.sal.core.message;

import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.sal.api.message.LocaleResolver;
import com.atlassian.sal.api.user.UserKey;

/**
 * Simple resolver that only supports the system default locale.
 *
 * @since 2.2.0
 */
public class SystemDefaultLocaleResolver implements LocaleResolver {
    @Override
    public Locale getLocale(HttpServletRequest request) {
        return getLocale();
    }

    @Override
    public Locale getLocale() {
        return Locale.getDefault();
    }

    @Override
    public Locale getLocale(UserKey userKey) {
        return Locale.getDefault();
    }

    @Override
    public Locale getApplicationLocale() {
        return Locale.getDefault();
    }

    @Override
    public Set<Locale> getSupportedLocales() {
        return new HashSet<>(Collections.singletonList(Locale.getDefault()));
    }
}
