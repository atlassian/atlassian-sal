package com.atlassian.sal.api.license;

import java.util.Collection;
import java.util.Locale;
import java.util.Set;
import java.util.SortedSet;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.PublicApi;
import com.atlassian.sal.api.i18n.InvalidOperationException;
import com.atlassian.sal.api.validate.MultipleLicensesValidationResult;
import com.atlassian.sal.api.validate.ValidationResult;

/**
 * Interface into the license system for the underlying application.
 * <p>
 * Licenses and products across Atlassian vary considerably with respect to the number of licenses that any given
 * product may accept (1-many), but also in terms of the number of products that may be granted within license (also
 * 1-many). This interface provides both a {@link com.atlassian.sal.api.license.SingleProductLicenseDetailsView single
 * product view} of license details (regardless of the actual number of licenses and products present), as well as a
 * {@link com.atlassian.sal.api.license.MultiProductLicenseDetails}.
 *
 * @see com.atlassian.sal.api.license.MultiProductLicenseDetails
 * @since 2.0
 */
@SuppressWarnings("UnusedDeclaration")
@PublicApi
public interface LicenseHandler {
    /**
     * Sets the license string for the currently running application.
     * <p>
     * This method will fire a {@link com.atlassian.sal.api.license.LicenseChangedEvent} upon successfully setting
     * the license.
     * <p>
     * This method is not suitable for platforms.
     *
     * @param license The raw license string
     * @throws IllegalArgumentException                if the license string is not a valid license
     * @throws java.lang.UnsupportedOperationException if this is a platform that allows multiple licenses
     * @deprecated Use {@link #addProductLicense(String, String)} instead. Since 3.0.
     */
    @Deprecated
    void setLicense(String license);

    /**
     * Use this to figure out if this host application uses a single global license, or if it is a platform that can take
     * multiple Product licenses.
     * <p>
     * Most applications would return false here but Fisheye/Crucible will return true, and JIRA 7.0 will return true.
     *
     * @return {@code true} if this application is a platform that accepts multiple product licenses, {@code false} if
     * it just takes a single global license.
     * @see #hostAllowsCustomProducts()
     * @since 3.0
     */
    boolean hostAllowsMultipleLicenses();

    /**
     * Returns true if it is possible to add custom products on top of this platform.
     * <p>
     * Most applications would return false here.
     * Confluence returns true because it has Spaces and Questions, JIRA 7.0 will return true, but Fisheye/Crucible will
     * return false - although it has separate licenses for FishEye and Crucible, you cannot add new custom products.
     *
     * @return {@code true} if this application is a platform that accepts multiple product licenses, {@code false} if
     * it just takes a single global license.
     * @see #hostAllowsMultipleLicenses()
     * @since 3.0
     */
    boolean hostAllowsCustomProducts();

    /**
     * Returns the list of currently available products in this host application whether these products are currently
     * licensed or not.
     *
     * <p>
     * For FishEye/Crucible this will return both "fisheye" and "crucible".
     * <br>
     * For JIRA, it will return the list of products that are currently installed (eg "com.atlassian.servicedesk").
     * <br>
     * For simple applications that only take a single license, it will return a single application name where that
     * name is the ID used by HAMS to define the application in the license eg "bamboo", "conf".
     * <br>
     *
     * @return the list of currently available products in this host application
     */
    Set<String> getProductKeys();

    /**
     * Adds the given product license to the host application.
     * <p>
     * For a platform that can take multiple license, the platform will figure out which product this is for and
     * replace the current license for that product if one exists.
     * <p>
     * This method will fire a {@link com.atlassian.sal.api.license.LicenseChangedEvent} upon successfully adding
     * the license.
     *
     * @param productKey The product to add this license to
     * @param license    The license string
     * @throws InvalidOperationException if the license string is not valid for any reason.
     *                                   The InvalidOperationException should be localised for the currently logged in user and consumers are
     *                                   encouraged to call {@link InvalidOperationException#getLocalizedMessage()}
     * @since 3.0
     */
    void addProductLicense(@Nonnull String productKey, @Nonnull String license) throws InvalidOperationException;

    /**
     * The addProductLicenses is similar to {@link LicenseHandler#addProductLicense(String, String)}
     * except that it able to verify and add multiple products in the same request.
     * Adds the given licenses to the host application.
     * <p>
     * For a platform that can take multiple licenses, the platform will figure out which product this is for and
     * replace the current license for that product if one exists.
     * <p>
     * This method will fire a {@link com.atlassian.sal.api.license.LicenseChangedEvent} upon successfully adding
     * the licenses.
     *
     * @param rawProductLicenses the licenses to add
     * @throws InvalidOperationException if the licenses are not valid
     * @since 4.2
     */
    void addProductLicenses(@Nonnull Set<RawProductLicense> rawProductLicenses) throws InvalidOperationException;

    /**
     * Removes the product license for the given productKey.
     * <p>
     * If the given product is in a multi-product license, it will remove that license and so all those products will
     * become unlicensed. You can use {@code #getAllProductLicenses} to find multi-product licenses.
     * <p>
     * This method will fire a {@link com.atlassian.sal.api.license.LicenseChangedEvent} upon successfully removing
     * the license.
     * <p>
     * If the passed productKey does not exist or is not licensed, the method will no-op and return successfully.
     *
     * @param productKey The product to remove the license of
     * @throws InvalidOperationException if the host application vetoes this operation - eg JIRA will not let you remove
     *                                   the very last license. The InvalidOperationException should be localised for
     *                                   the currently logged in user and consumers are encouraged to call
     *                                   {@link InvalidOperationException#getLocalizedMessage()}
     * @since 3.0
     */
    void removeProductLicense(@Nonnull String productKey) throws InvalidOperationException;

    /**
     * Validates that the given license is valid to add for the given product.
     *
     * @param productKey The product to add this license to
     * @param license    the raw license String
     * @param locale     locale of the end user - this is used to internationalise the error messages if any.
     */
    @Nonnull
    ValidationResult validateProductLicense(
            @Nonnull String productKey, @Nonnull String license, @Nullable Locale locale);

    /**
     * Validates that the given licenses are valid to add for the given product.
     *
     * @param rawProductLicenses The products to add these licenses to
     * @param locale             locale of the end user - this is used to internationalise the error messages if any.
     * @return Validation results
     * @since 4.2
     */
    @Nonnull
    MultipleLicensesValidationResult validateMultipleProductLicenses(
            @Nonnull Set<RawProductLicense> rawProductLicenses, @Nullable Locale locale);

    /**
     * Gets the server ID of the currently running application.  The server ID format is four quadruples of
     * alphanumeric characters, each separated by a dash ({@code -}).
     *
     * @return the server ID, or {@code null} if the server ID has not yet
     * been set for the currently running application.
     * @since 2.7
     */
    @Nullable
    String getServerId();

    /**
     * Gets the Support Entitlement Number (SEN) for the single license in the currently running application.
     * <p>
     * This method is not suitable for platforms because these may have multiple licenses and hence multiple SENs.
     *
     * @return the Support Entitlement Number, or {@code null} if there is no current support entitlement.
     * @throws java.lang.UnsupportedOperationException if this is a platform that allows multiple licenses
     * @since 2.7
     * @deprecated Get the license details and call {@link BaseLicenseDetails#getSupportEntitlementNumber()}. Since 3.0.
     */
    @Nullable
    @Deprecated
    String getSupportEntitlementNumber();

    /**
     * Gets the Support Entitlement Numbers (SENs) for all licenses in the currently running application.
     * The SENs are in an ordered set, guaranteeing to return the SEN in the same order until the installed license
     * state changes.
     * <p>
     * Note that licensed plugin SENs are not included in the results, unless they are being treated as application
     * licenses.
     *
     * @return an ordered set of all the SENs. The set does not contain any empty or null SEN strings. If there is no
     * SEN it will return an empty set. Non-platform products may return a set with only one SEN.
     * @since 3.0
     */
    @Nonnull
    SortedSet<String> getAllSupportEntitlementNumbers();

    /**
     * Returns the raw license String for a given product on a platform.
     *
     * @param productKey the product key.
     * @return the raw license String for the given product key.
     * @see #getProductLicenseDetails(String)
     * @since 3.0
     */
    @Nullable
    String getRawProductLicense(String productKey);

    /**
     * Returns an individual product view of the license details for the product identified by the given product key, or
     * null if the product does not exist in any license.
     *
     * @param productKey the product key.
     * @return the license details for the given product key.
     * @see #getRawProductLicense(String)
     * @see #decodeLicenseDetails(String)
     * @see #getAllProductLicenses()
     * @since 3.0
     */
    @Nullable
    SingleProductLicenseDetailsView getProductLicenseDetails(@Nonnull String productKey);

    /**
     * Returns all the product licenses on a platform.
     * <p>
     * A platform may return multiple licenses, a simple application will return 0 or 1.
     *
     * @return all the product licenses on a platform.
     * @see #getProductLicenseDetails(String)
     * @since 3.0
     */
    @Nonnull
    Collection<MultiProductLicenseDetails> getAllProductLicenses();

    /**
     * Decodes a Platform Product License from the raw license String.
     * This can be used to validate an entered license String before attempting to save it to the application.
     *
     * @param license the raw license String
     * @return the license details for the given product key.
     * @throws java.lang.IllegalArgumentException if the license string is not able to be decoded in this host application
     * @see #getProductLicenseDetails(String)
     * @since 3.0
     */
    @Nonnull
    MultiProductLicenseDetails decodeLicenseDetails(@Nonnull String license);
}
