package com.atlassian.sal.api.validate;

/**
 * All warning types of the license verification's result.
 *
 * @since 4.2
 */
public enum LicenseWarningCode {
    UNKNOWN,

    /**
     * The license has incorrect the number of users/seats.
     */
    ACTIVATE_USERS
}
