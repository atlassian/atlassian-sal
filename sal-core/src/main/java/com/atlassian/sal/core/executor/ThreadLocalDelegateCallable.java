package com.atlassian.sal.core.executor;

import java.util.concurrent.Callable;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;

import static java.util.Objects.requireNonNull;

/**
 * A wrapping callable that copies the thread local state into the calling code
 *
 * @since 2.0
 */
class ThreadLocalDelegateCallable<C, T> implements Callable<T> {
    private final Callable<T> delegate;
    private final ThreadLocalContextManager<C> manager;
    private final C context;
    private final ClassLoader contextClassLoader;

    /**
     * Create a new callable
     *
     * @param manager  The context manager to get the context from
     * @param delegate The callable to delegate to
     */
    ThreadLocalDelegateCallable(ThreadLocalContextManager<C> manager, Callable<T> delegate) {
        this.delegate = requireNonNull(delegate);
        this.manager = requireNonNull(manager);
        this.context = manager.getThreadLocalContext();
        this.contextClassLoader = Thread.currentThread().getContextClassLoader();
    }

    public T call() throws Exception {
        ClassLoader oldContextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
            manager.setThreadLocalContext(context);
            return delegate.call();
        } finally {
            Thread.currentThread().setContextClassLoader(oldContextClassLoader);
            manager.clearThreadLocalContext();
        }
    }
}
