package com.atlassian.sal.core.features;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.UnaryOperator;
import javax.annotation.Nullable;

import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.ImmutableSet;
import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.sal.api.features.SiteDarkFeaturesStorage;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import static java.util.Objects.requireNonNull;

/**
 * Default implementation responsible for persisting site wide enabled dark features. The general contract is that
 * reading is fast while updating is more expensive. Uses the plugin settings to store the dark features. Should be
 * able to store up to 1.980 unique dark feature keys with each key about 50 characters long.
 *
 * @since 2.10
 */
public class DefaultSiteDarkFeaturesStorage implements SiteDarkFeaturesStorage {
    private static final String SITE_WIDE_DARK_FEATURES = "atlassian.sitewide.dark.features";

    private final ResettableLazyReference<Set<String>> cache = new ResettableLazyReference<Set<String>>() {
        @Override
        protected Set<String> create() {
            return ImmutableSet.copyOf(load());
        }
    };

    private final PluginSettingsFactory pluginSettingsFactory;

    public DefaultSiteDarkFeaturesStorage(final PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public boolean contains(String featureKey) {
        final String trimmedFeatureKey =
                requireNonNull(StringUtils.trimToNull(featureKey), "featureKey must not be blank");
        return cache.get().contains(trimmedFeatureKey);
    }

    @Override
    public void enable(final String featureKey) {
        final String trimmedFeatureKey =
                requireNonNull(StringUtils.trimToNull(featureKey), "featureKey must not be blank");
        if (!cache.get().contains(trimmedFeatureKey)) {
            update(addFeatureKey(trimmedFeatureKey));
            cache.reset();
        }
    }

    @Override
    public void disable(final String featureKey) {
        final String trimmedFeatureKey =
                requireNonNull(StringUtils.trimToNull(featureKey), "featureKey must not be blank");
        if (cache.get().contains(trimmedFeatureKey)) {
            update(removeFeatureKey(trimmedFeatureKey));
            cache.reset();
        }
    }

    @Override
    public Set<String> getEnabledDarkFeatureSet() {
        return cache.get();
    }

    /**
     * Update the list of stored dark feature keys. The workflow is:
     * <ol>
     * <li>Load the old list from storage</li>
     * <li>Apply the transformation function</li>
     * <li>Store the new list</li>
     * </ol>
     *
     * @param transformer the function to be applied on the exist list of enabled dark feature keys
     */
    private synchronized void update(final UnaryOperator<List<String>> transformer) {
        /*
         * Using a function allows to defer the actual list manipulation to a point when the thread
         * got the proper locks.
         */
        final List<String> storedFeatureKeys = load();
        final List<String> updatedFeatureKeys = transformer.apply(storedFeatureKeys);
        store(updatedFeatureKeys);
    }

    private synchronized List<String> load() {
        final PluginSettings globalSettings = pluginSettingsFactory.createGlobalSettings();
        final Object value = globalSettings.get(SITE_WIDE_DARK_FEATURES);
        return extractFeatureKeys(value);
    }

    private List<String> extractFeatureKeys(@Nullable final Object value) {
        final LinkedList<String> storedFeatureKeys = new LinkedList<>();
        if (value instanceof List) {
            final List list = List.class.cast(value);
            for (final Object listItem : list) {
                if (listItem instanceof String) {
                    storedFeatureKeys.addLast(String.class.cast(listItem));
                }
            }
        }
        return storedFeatureKeys;
    }

    private synchronized void store(final List<String> updatedFeatureKeys) {
        final PluginSettings globalSettings = pluginSettingsFactory.createGlobalSettings();
        globalSettings.put(SITE_WIDE_DARK_FEATURES, updatedFeatureKeys);
    }

    private UnaryOperator<List<String>> addFeatureKey(final String featureKey) {
        return storedFeatureKeys -> {
            if (storedFeatureKeys == null) {
                return null;
            }

            final List<String> result = new ArrayList<>(storedFeatureKeys);
            if (!storedFeatureKeys.contains(featureKey)) {
                result.add(featureKey);
            }
            return result;
        };
    }

    private UnaryOperator<List<String>> removeFeatureKey(final String featureKey) {
        return storedFeatureKeys -> {
            if (storedFeatureKeys == null) {
                return storedFeatureKeys;
            }

            final List<String> result = new ArrayList<>(storedFeatureKeys);
            result.remove(featureKey);
            return result;
        };
    }
}
