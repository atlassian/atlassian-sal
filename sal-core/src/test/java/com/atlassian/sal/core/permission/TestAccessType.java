package com.atlassian.sal.core.permission;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import com.atlassian.annotations.security.AdminOnly;
import com.atlassian.annotations.security.AnonymousSiteAccess;
import com.atlassian.annotations.security.LicensedOnly;
import com.atlassian.annotations.security.SystemAdminOnly;
import com.atlassian.annotations.security.UnlicensedSiteAccess;
import com.atlassian.annotations.security.UnrestrictedAccess;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TestAccessType {

    @Test
    void testClassWithAnnotatedMethodsThatDifferByParamsMethodByName() {
        Class<?> clazz = ClassWithAnnotatedMethods.class;
        AccessType accessType1 = AccessType.getAccessType(clazz, "testMethod", int.class, String.class);
        AccessType accessType2 = AccessType.getAccessType(clazz, "testMethod");
        assertEquals(AccessType.ADMIN_ONLY, accessType1);
        assertEquals(AccessType.SYSTEM_ADMIN_ONLY, accessType2);
    }

    @Test
    void testClassWithAnnotatedMethodsThatDifferByParams() throws NoSuchMethodException {
        Class<?> clazz = ClassWithAnnotatedMethods.class;
        AccessType accessType1 =
                AccessType.getAccessType(clazz.getDeclaredMethod("testMethod", int.class, String.class));
        AccessType accessType2 = AccessType.getAccessType(clazz.getDeclaredMethod("testMethod"));
        assertEquals(AccessType.ADMIN_ONLY, accessType1);
        assertEquals(AccessType.SYSTEM_ADMIN_ONLY, accessType2);
    }

    @Test
    void testMethodAnnotationTakesPrecedenceOverClassAnnotationMethodByName() {
        Class<?> clazz = AnnotatedClassWithAnnotatedMethod.class;
        AccessType accessType = AccessType.getAccessType(clazz, "testMethod");
        assertEquals(AccessType.SYSTEM_ADMIN_ONLY, accessType);
    }

    @Test
    void testWrongAnnotationIsNotRead() {
        Class<?> clazz = DeprecatedClass.class;
        AccessType accessType = AccessType.getAccessType(clazz, "testMethod");
        assertEquals(AccessType.EMPTY, accessType);
    }

    @Test
    void testAdditionalMappingByMethodName() {
        Class<?> clazz = DeprecatedClass.class;
        Map<String, AccessType> additionalMapping = new HashMap<>();
        additionalMapping.put(Deprecated.class.getName(), AccessType.SYSTEM_ADMIN_ONLY);
        AccessType accessType = AccessType.getAccessType(clazz, "testMethod", additionalMapping);
        assertEquals(AccessType.SYSTEM_ADMIN_ONLY, accessType);
    }

    @Test
    void testAdditionalMapping() throws NoSuchMethodException {
        Class<?> clazz = DeprecatedClass.class;
        Map<String, AccessType> additionalMapping = new HashMap<>();
        additionalMapping.put(Deprecated.class.getName(), AccessType.SYSTEM_ADMIN_ONLY);
        AccessType accessType = AccessType.getAccessType(clazz.getDeclaredMethod("testMethod"), additionalMapping);
        assertEquals(AccessType.SYSTEM_ADMIN_ONLY, accessType);
    }

    @Test
    void testNoMethodFoundFallbackToClass() {
        Class<?> clazz = AnnotatedClassWithAnnotatedMethod.class;
        AccessType accessType = AccessType.getAccessType(clazz, "thisMethodDoesNotExist");
        assertEquals(AccessType.ADMIN_ONLY, accessType);
    }

    @Test
    void testNoMethodFoundFallbackToClassClassEmpty() {
        Class<?> clazz = ClassWithAnnotatedMethods.class;
        AccessType accessType = AccessType.getAccessType(clazz, "thisMethodDoesNotExist");
        assertEquals(AccessType.EMPTY, accessType);
    }

    @Test
    void testDerivedClassWithNoAnnotation() {
        assertThrows(
                IllegalStateException.class,
                () -> AccessType.getAccessType(ClassWithNoAnnotationDerivedFromClassWithAnnotation.class, "testMethod"),
                "Class com.atlassian.sal.core.permission.TestAccessType$ClassWithNoAnnotationDerivedFromClassWithAnnotation, "
                        + "which has no security annotation, inherits from "
                        + "class com.atlassian.sal.core.permission.TestAccessType$SystemAdminOnlyClass, "
                        + "which has access level set to SYSTEM_ADMIN_ONLY. "
                        + "A class that extends an annotated class must be explicitly annotated itself "
                        + "to avoid false assumption about annotation inheritance. "
                        + "Add a security annotation to "
                        + "class com.atlassian.sal.core.permission.TestAccessType$ClassWithNoAnnotationDerivedFromClassWithAnnotation");
    }

    @Test
    void testNullClass() {
        assertThrows(NullPointerException.class, () -> AccessType.getAccessType(null, "testMethod"));
    }

    @Test
    void testClassWithMultipleAnnotations() {
        assertThrows(
                IllegalStateException.class,
                () -> AccessType.getAccessType(ClassWithMultipleAnnotations.class, "testMethod"),
                "Multiple security annotations found: ADMIN_ONLY and SYSTEM_ADMIN_ONLY. Only one security annotation is allowed on an element.");
    }

    @Test
    void testMethodWithMultipleAnnotations() {
        IllegalStateException exception = assertThrows(
                IllegalStateException.class,
                () -> AccessType.getAccessType(ClassWithMethodWithMultipleAnnotations.class, "testMethod"));

        assertEquals(
                exception.getMessage(),
                "Multiple security annotations found: ADMIN_ONLY and SYSTEM_ADMIN_ONLY. Only one security annotation is allowed on an element.");
    }

    @Test
    void testClassLoaderAreIgnoredWhenComparingAnnotations() throws ClassNotFoundException {
        Class<?> clazz = new DifferentClassLoader().loadClass(ClassWithDifferentClassloader.class.getName());
        AccessType accessType = AccessType.getAccessType(clazz, "testMethod");
        assertEquals(AccessType.SYSTEM_ADMIN_ONLY, accessType);
    }

    @ParameterizedTest
    @EnumSource(AnnotatedClasses.class)
    void testAllValidAnnotationsAreRead(AnnotatedClasses annotatedClass) {
        Class<?> clazz = annotatedClass.getClazz();
        String methodName = annotatedClass.getMethodName();
        AccessType expectedAccessType = annotatedClass.getExpectedAccessType();

        AccessType accessType = AccessType.getAccessType(clazz, methodName);
        assertEquals(expectedAccessType, accessType);
    }

    enum AnnotatedClasses {
        ADMIN_ONLY_CLASS(AdminOnlyClass.class, "testMethod", AccessType.ADMIN_ONLY),
        ANONYMOUS_SITE_ACCESS_CLASS(AnonymousSiteAccessClass.class, "testMethod", AccessType.ANONYMOUS_SITE_ACCESS),
        UNLICENSED_SITE_ACCESS_CLASS(UnlicensedSiteAccessClass.class, "testMethod", AccessType.UNLICENSED_SITE_ACCESS),
        LICENSED_ONLY_CLASS(LicensedOnlyClass.class, "testMethod", AccessType.LICENSED_ONLY),
        SYSTEM_ADMIN_ONLY_CLASS(SystemAdminOnlyClass.class, "testMethod", AccessType.SYSTEM_ADMIN_ONLY),
        UNRESTRICTED_ACCESS_CLASS(UnrestrictedAccessClass.class, "testMethod", AccessType.UNRESTRICTED_ACCESS),
        ADMIN_ONLY_METHOD(AdminOnlyMethod.class, "testMethod", AccessType.ADMIN_ONLY),
        ANONYMOUS_SITE_ACCESS_METHOD(AnonymousSiteAccessMethod.class, "testMethod", AccessType.ANONYMOUS_SITE_ACCESS),
        UNLICENSED_SITE_ACCESS_METHOD(
                UnlicensedSiteAccessMethod.class, "testMethod", AccessType.UNLICENSED_SITE_ACCESS),
        LICENSED_ONLY_METHOD(LicensedOnlyMethod.class, "testMethod", AccessType.LICENSED_ONLY),
        SYSTEM_ADMIN_ONLY_METHOD(SystemAdminOnlyMethod.class, "testMethod", AccessType.SYSTEM_ADMIN_ONLY),
        UNRESTRICTED_ACCESS_METHOD(UnrestrictedAccessMethod.class, "testMethod", AccessType.UNRESTRICTED_ACCESS);
        private final Class<?> clazz;
        private final String methodName;
        private final AccessType expectedAccessType;

        AnnotatedClasses(Class<?> clazz, String methodName, AccessType expectedAccessType) {
            this.clazz = clazz;
            this.methodName = methodName;
            this.expectedAccessType = expectedAccessType;
        }

        public Class<?> getClazz() {
            return clazz;
        }

        public String getMethodName() {
            return methodName;
        }

        public AccessType getExpectedAccessType() {
            return expectedAccessType;
        }
    }

    static class ClassWithAnnotatedMethods {
        @AdminOnly
        public void testMethod(int a, String b) {}

        @SystemAdminOnly
        public void testMethod() {}
    }

    @AdminOnly
    static class AnnotatedClassWithAnnotatedMethod {
        @SystemAdminOnly
        public void testMethod() {}
    }

    @AdminOnly
    static class AdminOnlyClass {
        public void testMethod() {}
    }

    static class AdminOnlyMethod {
        @AdminOnly
        public void testMethod() {}
    }

    @AnonymousSiteAccess
    static class AnonymousSiteAccessClass {
        public void testMethod() {}
    }

    @AnonymousSiteAccess
    static class AnonymousSiteAccessMethod {
        public void testMethod() {}
    }

    @LicensedOnly
    static class LicensedOnlyClass {
        public void testMethod() {}
    }

    static class LicensedOnlyMethod {

        @LicensedOnly
        public void testMethod() {}
    }

    @SystemAdminOnly
    static class SystemAdminOnlyClass {
        public void testMethod() {}
    }

    static class SystemAdminOnlyMethod {
        @SystemAdminOnly
        public void testMethod() {}
    }

    @UnlicensedSiteAccess
    static class UnlicensedSiteAccessClass {
        public void testMethod() {}
    }

    static class UnlicensedSiteAccessMethod {
        @UnlicensedSiteAccess
        public void testMethod() {}
    }

    @UnrestrictedAccess
    static class UnrestrictedAccessClass {
        public void testMethod() {}
    }

    static class UnrestrictedAccessMethod {
        @UnrestrictedAccess
        public void testMethod() {}
    }

    /**
     * Has no security annotation as the class annotation is not a security annotation.
     */
    @Deprecated
    static class DeprecatedClass {
        public void testMethod() {}
    }

    static class ClassWithNoAnnotationDerivedFromClassWithAnnotation extends SystemAdminOnlyClass {
        public void testMethod() {}
    }

    @AdminOnly
    @SystemAdminOnly
    static class ClassWithMultipleAnnotations {

        public void testMethod() {}
    }

    static class ClassWithMethodWithMultipleAnnotations {

        @AdminOnly
        @SystemAdminOnly
        public void testMethod() {}
    }

    static class ClassWithDifferentClassloader {

        @SystemAdminOnly
        public void testMethod() {}
    }

    private static final class DifferentClassLoader extends ClassLoader {

        private final Set<String> supportedNames = Stream.of(
                        SystemAdminOnly.class.getName(), ClassWithDifferentClassloader.class.getName())
                .collect(Collectors.toSet());

        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            if (!supportedNames.contains(name)) {
                return super.loadClass(name);
            }
            try {
                InputStream in = ClassLoader.getSystemResourceAsStream(name.replace(".", "/") + ".class");
                byte[] a = new byte[10000];
                int len = in.read(a);
                in.close();
                return defineClass(name, a, 0, len);
            } catch (IOException e) {
                throw new ClassNotFoundException();
            }
        }
    }
}
