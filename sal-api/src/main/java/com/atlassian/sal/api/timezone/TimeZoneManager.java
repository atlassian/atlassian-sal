package com.atlassian.sal.api.timezone;

import java.util.TimeZone;
import javax.annotation.Nonnull;

import com.atlassian.sal.api.user.UserKey;

/**
 * @since v2.6.0
 */
public interface TimeZoneManager {

    /**
     * Returns the time zone of the logged in user.
     * NB: This is guaranteed to return a non-null value.
     * If no user is logged in (anonymous user) or the system doesn't support time zone configuration or no specific time zone is configured,
     * it should still return a time zone. (e.g. the default time zone for the system).
     *
     * @return the user's time zone. Should never return null.
     */
    @Nonnull
    TimeZone getUserTimeZone();

    /**
     * Returns the time zone of the given user.
     *
     * NB: This is guaranteed to return a non-null value.
     * If given user doesn't exists or invalid or the system doesn't support time zone configuration or no specific time zone is configured,
     * for the given user then method still returns a time zone. (e.g. the default time zone for the system).
     *
     * @param user to return time zone for
     * @return the user's time zone or default time if user not found, is invalid or has no specific time zone configured
     * @throws NullPointerException if given user is <code>null</code>
     * @since v2.13.0
     */
    @Nonnull
    TimeZone getUserTimeZone(@Nonnull UserKey user);

    /**
     * Returns the default time zone for the application.
     * NB: This is guaranteed to return a non-null value.
     *
     * If the system doesn't support time zone configuration, it should still return a time zone (e.g. the JVM time zone).
     *
     * @return the default time zone of the system. Should never return null.
     */
    @Nonnull
    TimeZone getDefaultTimeZone();
}
