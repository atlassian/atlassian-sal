# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [5.2.0]

### Deprecated
- `sal-core`, `sal-spi`, and `sal-spring` will be limited to internal product usage only

## [5.1.3] - Released

### Changed
- [REST-451] Introduce MarshallingRequestFactory to resolve ambiguous injection

## [5.1.0] - Released

### Changed
- [SAL-390] New SAL APIs to allow querying and enforcement of unlicensed and anonymous access
  - See [API change overview here](https://hello.atlassian.net/l/cp/9TacQpSH) for details and expected product implementations
  - All DC products will need to, at the minimum, implement the following new APIs:
    - UserManager#isAnonymousAccessEnabled
    - UserManager#isLicensed
    - UserManager#isLimitedUnlicensedAccessEnabled
  - It is also recommended to override the default implementation of the following API to ensure it is performant and considers product specific factors such as whether user is not disabled:
    - UserManager#isLimitedUnlicensedUser
- [BSP-4442] Update spring beans
- [ITASM2-363] Fix transactionalExecutor for Java 17

## [5.0.0] - Released

### Changed
- [BSP-3371] update Atlassian Plugins - Platform 6 milestone 2
- [BSP-3424] Bump Platform 6 version
- [ITASM3-70] Remove usage of MetricTag#of
- [BSP-3594] Prepare for stable Platform 6 release
- [ITASM3-56] Check for executor pluginkey
- [ITASM3-58] Downgrade version of profiling

## [4.7.0] - Released

### Changed
- [ITAS-209] Add transactional executor metric
- [ITAS-98] Add metrics to SAL http requests

## [4.6.0] - Released

### Changed
- [BSP-2537] Remove Platform POM
- [BSP-2692] Fix add-on upgrade tasks
- [BSP-3016] Add Guava-free APIs

## [4.5.0] - Released

### Changed
- [BSP-1445] Remove deprecated annotations
- [BSP-1692] Added missing test assertions
- [BSP-1691] Added coverage exclusions
- [BSP-1945] Add SortPom verify plugin
- [BSP-1946] Bump httpclient version to 4.5.13
- [BSP-2163] Improve code reliability score
- [BSP-2245] Add renovate
- [BSP-2265] Add security-assistant
- [BSP-2362] Split JAXB property
- [BSP-2398] Upgrade commons codec version
- [SAL-375] Skip invalid PluginUpgradeTasks
- [BSP-1056] Update branching model
- [BSP-2459] Add request type patch
- [BSP-2460] Make hamcrest dependencies test scope

## [4.4.0] - Released

### Changed
- [BSP-1211] Enabling code coverage reports in CI
- [SAL-383] Add API to allow for resolution of current application locale and file encoding.
- [SAL-380] Correct hamcrest dependencies to use test scope

## [4.3.0] - Released

### Changed
- [BSP-1033] Jacoco and Sonar support

## [4.2.0] - Released

### Changed
- [SAL-379] Support validation, add/update multiple licenses.

## [4.1.0] - Released

### Changed
- [SAL-378] Introduce getLocalHomeDirectory and getSharedHomeDirectory methods to ApplicationProperties
- [BSP-170] Add mvnvm.properties file
- [BSP-283] Tidied POMs
- [BSP-271] Update README
- [BSP-36] Update to final Platform 5 POM
- [BSP-559] Skip test dependencies for SourceClear

## [4.0.0] - Released

### Changed
- [SAL-373] Now compatible with Java 9, 10 and 11
- [SAL-373] Now depends on `commons-lang3` instead of `commons-lang`
- [SAL-373] Now requires Platform 5
- [SAL-371] Introduce PermissionEnforcer
- [BSP-28] Bump Fuge version
- [BSP-6] Make version OSGi compatible
- [BSP-36] Bumped the Platform, AMPS and BeeHive
- [BSP-66] Fixed TestHttpClientRequestSSL

[SAL-373]: https://ecosystem.atlassian.net/browse/SAL-373
[SAL-378]: https://ecosystem.atlassian.net/browse/SAL-378
[SAL-379]: https://ecosystem.atlassian.net/browse/SAL-379
[SAL-375]: https://ecosystem.atlassian.net/browse/SAL-375
[SAL-380]: https://ecosystem.atlassian.net/browse/SAL-380
[SAL-383]: https://ecosystem.atlassian.net/browse/SAL-383
[SAL-390]: https://ecosystem.atlassian.net/browse/SAL-390
[SAL-371]: https://ecosystem.atlassian.net/browse/SAL-371
[BSP-4442]: https://bulldog.internal.atlassian.com/browse/BSP-4442
[BSP-1033]: https://bulldog.internal.atlassian.com/browse/BSP-1033
[BSP-2537]: https://bulldog.internal.atlassian.com/browse/BSP-2537
[BSP-1945]: https://bulldog.internal.atlassian.com/browse/BSP-1945
[BSP-2692]: https://bulldog.internal.atlassian.com/browse/BSP-2692
[BSP-3016]: https://bulldog.internal.atlassian.com/browse/BSP-3016
[BSP-1445]: https://bulldog.internal.atlassian.com/browse/BSP-1445
[BSP-1692]: https://bulldog.internal.atlassian.com/browse/BSP-1692
[BSP-1691]: https://bulldog.internal.atlassian.com/browse/BSP-1691
[BSP-1946]: https://bulldog.internal.atlassian.com/browse/BSP-1946
[BSP-2163]: https://bulldog.internal.atlassian.com/browse/BSP-2163
[BSP-2245]: https://bulldog.internal.atlassian.com/browse/BSP-2245
[BSP-2265]: https://bulldog.internal.atlassian.com/browse/BSP-2265
[BSP-2362]: https://bulldog.internal.atlassian.com/browse/BSP-2362
[BSP-2398]: https://bulldog.internal.atlassian.com/browse/BSP-2398
[BSP-1056]: https://bulldog.internal.atlassian.com/browse/BSP-1056
[BSP-2459]: https://bulldog.internal.atlassian.com/browse/BSP-2459
[BSP-2460]: https://bulldog.internal.atlassian.com/browse/BSP-2460
[BSP-1211]: https://bulldog.internal.atlassian.com/browse/BSP-1211
[BSP-170]: https://bulldog.internal.atlassian.com/browse/BSP-170
[BSP-283]: https://bulldog.internal.atlassian.com/browse/BSP-283
[BSP-271]: https://bulldog.internal.atlassian.com/browse/BSP-271
[BSP-36]: https://bulldog.internal.atlassian.com/browse/BSP-36
[BSP-559]: https://bulldog.internal.atlassian.com/browse/BSP-559
[BSP-28]: https://bulldog.internal.atlassian.com/browse/BSP-28
[BSP-6]: https://bulldog.internal.atlassian.com/browse/BSP-6
[BSP-66]: https://bulldog.internal.atlassian.com/browse/BSP-66
[BSP-3371]: https://bulldog.internal.atlassian.com/browse/BSP-3371
[BSP-3424]: https://bulldog.internal.atlassian.com/browse/BSP-3424
[BSP-3594]: https://bulldog.internal.atlassian.com/browse/BSP-3594
[ITAS-209]: https://bulldog.internal.atlassian.com/browse/ITAS-209
[ITAS-98]: https://bulldog.internal.atlassian.com/browse/ITAS-98
[ITASM2-363]: https://bulldog.internal.atlassian.com/browse/ITASM2-363
[ITASM3-56]: https://bulldog.internal.atlassian.com/browse/ITASM3-56
[ITASM3-58]: https://bulldog.internal.atlassian.com/browse/ITASM3-58
[ITASM3-70]: https://bulldog.internal.atlassian.com/browse/ITASM3-70
