package com.atlassian.sal.api.features;

import java.util.Map;
import java.util.Optional;
import javax.annotation.Nullable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.webresource.api.QueryParams;
import com.atlassian.webresource.api.url.UrlBuilder;
import com.atlassian.webresource.spi.condition.UrlReadingCondition;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.sal.api.features.DarkFeatureEnabledCondition.DEFAULT_STATE;

@RunWith(MockitoJUnitRunner.class)
public class TestDarkFeatureEnabledCondition {
    private static final Map<String, Object> EMPTY_CONTEXT = emptyMap();
    private static final String TEST_FEATURE = "test.feature";

    @Mock
    private DarkFeatureManager darkFeatureManager;

    @Test
    public void givenFeatureEnabled_whenSyncfullyChecked_thenDisplay() {
        when(darkFeatureManager.isEnabledForCurrentUser(TEST_FEATURE)).thenReturn(Optional.of(TRUE));
        assertTrue(createAndInitWithFeatureKey(TEST_FEATURE).shouldDisplay(EMPTY_CONTEXT));
    }

    @Test
    public void givenFeatureDisabled_whenSyncfullyChecked_thenNotDisplay() {
        when(darkFeatureManager.isEnabledForCurrentUser(TEST_FEATURE)).thenReturn(Optional.of(FALSE));
        assertFalse(createAndInitWithFeatureKey(TEST_FEATURE).shouldDisplay(EMPTY_CONTEXT));
    }

    @Test
    public void givenFeatureManagerBlowsUp_whenChecked_thenReturnDefaultValueGracefully() {
        when(darkFeatureManager.isEnabledForCurrentUser(anyString())).thenThrow(new RuntimeException());
        assertEquals(DEFAULT_STATE, createAndInitWithFeatureKey(TEST_FEATURE).shouldDisplay(EMPTY_CONTEXT));
    }

    @Test
    public void givenNullContext_whenSyncfullyChecked_thenReturnDefaultValueGracefully() {
        assertEquals(
                DEFAULT_STATE, createAndInitWithFeatureKey(TEST_FEATURE).shouldDisplay((Map<String, Object>) null));
    }

    /**
     * Yes, we're testing two methods, but they need to work together, and it's better to test that explicitly rather
     * than implicitly
     */
    @Test
    public void givenFeatureEnabled_whenAsyncChecked_thenDisplay() {
        final UrlReadingCondition condition = createAndInitWithFeatureKey(TEST_FEATURE);

        when(darkFeatureManager.isEnabledForCurrentUser(TEST_FEATURE)).thenReturn(Optional.of(TRUE));
        final UrlBuilder urlBuilder = mock(UrlBuilder.class);
        condition.addToUrl(urlBuilder);
        verify(urlBuilder).addToQueryString(TEST_FEATURE, "true");

        final QueryParams queryParams = mock(QueryParams.class);
        when(queryParams.get(TEST_FEATURE)).thenReturn("true");
        assertTrue(condition.shouldDisplay(queryParams));
    }

    /**
     * Yes, we're testing two methods, but they need to work together, and it's better to test that explicitly rather
     * than implicitly
     */
    @Test
    public void givenFeatureDisabled_whenAsyncChecked_thenNotDisplay() {
        final UrlReadingCondition condition = createAndInitWithFeatureKey(TEST_FEATURE);

        when(darkFeatureManager.isEnabledForCurrentUser(TEST_FEATURE)).thenReturn(Optional.of(FALSE));
        final UrlBuilder urlBuilder = mock(UrlBuilder.class);
        condition.addToUrl(urlBuilder);
        verify(urlBuilder).addToQueryString(TEST_FEATURE, "false");

        final QueryParams queryParams = mock(QueryParams.class);
        when(queryParams.get(TEST_FEATURE)).thenReturn("false");
        assertFalse(condition.shouldDisplay(queryParams));
    }

    @Test
    public void givenFeatureStateUnknown_whenAsyncChecked_thenReturnDefaultValueGracefully() {
        final QueryParams queryParams = mock(QueryParams.class);
        assertEquals(DEFAULT_STATE, createCondition().shouldDisplay(queryParams));
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void givenInvalidFeatureKey_whenInit_thenThrowToAvoidSurprise() {
        createAndInitWithFeatureKey("   invalid   ");
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void givenNullFeatureKey_whenInit_thenThrowToAvoidSurprise() {
        createAndInitWithFeatureKey(null);
    }

    @Test(expected = InvalidFeatureKeyException.class)
    public void givenEmptyFeatureKey_whenInit_thenThrowToAvoidSurprise() {
        createAndInitWithFeatureKey("");
    }

    @Test(expected = PluginParseException.class)
    public void givenMissingFeatureKey_whenInit_thenThrowToAvoidSurprise() {
        createCondition().init(emptyMap());
    }

    private DarkFeatureEnabledCondition createAndInitWithFeatureKey(@Nullable final String featureKey) {
        final DarkFeatureEnabledCondition condition = createCondition();
        condition.init(singletonMap("featureKey", featureKey));
        return condition;
    }

    private DarkFeatureEnabledCondition createCondition() {
        return new DarkFeatureEnabledCondition(darkFeatureManager);
    }
}
