package com.atlassian.sal.core.lifecycle;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import io.atlassian.fugue.Pair;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.events.PluginDisablingEvent;
import com.atlassian.plugin.event.events.PluginFrameworkShuttingDownEvent;
import com.atlassian.sal.api.lifecycle.LifecycleAware;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestDefaultLifecycleManagerOnStopAware extends TestDefaultLifecycleManagerBase {
    @Before
    @Override
    public void setUp() throws InvalidSyntaxException {
        super.setUp();
        defaultLifecycleManager.afterPropertiesSet();
    }

    @Test
    public void onStopNotCalledPrematurely() {
        final LifecycleAware deathAware = getLifecycleAwareStarted();

        verify(deathAware, never()).onStop();
    }

    @Test
    public void onStopCalledWhenFrameworkShuttingDown() {
        final LifecycleAware deathAware = getLifecycleAwareStarted();

        defaultLifecycleManager.onPluginFrameworkShuttingDown(
                new PluginFrameworkShuttingDownEvent(pluginController, pluginAccessor));
        verify(deathAware).onStop();
    }

    @Test
    public void onStopWillNotBreakLegacyPlugins() throws IllegalAccessException, InstantiationException {
        final LifecycleAware legacyLifecycleAware = LegacyLifecycleAwareGenerator.newInstance();

        registerService(pluginBundle, legacyLifecycleAware);
        enablePlugin(pluginKey);

        // Should not be called yet, as things haven't started
        assertThat(notifyOnStartCalled, is(0));

        isApplicationSetup = true;
        defaultLifecycleManager.start();

        // Check for start event
        assertThat(notifyOnStartCalled, is(1));

        defaultLifecycleManager.onPluginFrameworkShuttingDown(
                new PluginFrameworkShuttingDownEvent(pluginController, pluginAccessor));
    }

    @Test
    public void onStopCalledOnceWhenFrameworkShuttingDownCalledTwice() {
        final LifecycleAware deathAware = getLifecycleAwareStarted();

        defaultLifecycleManager.onPluginFrameworkShuttingDown(
                new PluginFrameworkShuttingDownEvent(pluginController, pluginAccessor));
        defaultLifecycleManager.onPluginFrameworkShuttingDown(
                new PluginFrameworkShuttingDownEvent(pluginController, pluginAccessor));
        verify(deathAware).onStop();
    }

    @Test
    public void onStopCalledWhenPluginDisabling() {
        final LifecycleAware deathAware = getLifecycleAwareStarted();
        defaultLifecycleManager.onPluginDisabling(new PluginDisablingEvent(getMyPlugin()));
        verify(deathAware).onStop();
    }

    @Test
    public void onStopCalledOnceWhenPluginDisablingCalledTwice() {
        final LifecycleAware deathAware = getLifecycleAwareStarted();
        defaultLifecycleManager.onPluginDisabling(new PluginDisablingEvent(getMyPlugin()));
        defaultLifecycleManager.onPluginDisabling(new PluginDisablingEvent(getMyPlugin()));
        verify(deathAware).onStop();
    }

    @Test
    public void onStopNotCalledWhenPluginDisablingIsForAnotherPlugin() {
        final LifecycleAware deathAware = getLifecycleAwareStarted();
        defaultLifecycleManager.onPluginDisabling(new PluginDisablingEvent(getPlugin(pluginKey + ".not-my-event")));
        verify(deathAware, never()).onStop();
    }

    @Test
    public void onStopCalledWhenServiceUnregistered() {
        Pair<LifecycleAware, ServiceReference<LifecycleAware>> started = getLifecycleAwareStartedWithService();
        unregisterService(started.right());
        verify(started.left()).onStop();
    }

    private Plugin getPlugin(String key) {
        Plugin plugin = mock(Plugin.class);
        when(plugin.getKey()).thenReturn(key);
        return plugin;
    }

    private Plugin getMyPlugin() {
        return getPlugin(pluginKey);
    }

    private Pair<LifecycleAware, ServiceReference<LifecycleAware>> getLifecycleAwareStartedWithService() {
        final Pair<LifecycleAware, ServiceReference<LifecycleAware>> started =
                mockLifecycleAwareAndRegisterService(pluginBundle);
        enablePlugin(pluginKey);
        isApplicationSetup = true;

        defaultLifecycleManager.start();
        return started;
    }

    private LifecycleAware getLifecycleAwareStarted() {
        return getLifecycleAwareStartedWithService().left();
    }

    private Pair<LifecycleAware, ServiceReference<LifecycleAware>> mockLifecycleAwareAndRegisterService(
            final Bundle bundle) {
        return mockLifecycleAwareAndRegisterService(bundle, LifecycleAware.class);
    }
}
