package com.atlassian.sal.spring.connection;

import java.sql.Connection;

import org.springframework.transaction.PlatformTransactionManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import io.atlassian.fugue.Option;

import com.atlassian.sal.api.rdbms.ConnectionCallback;
import com.atlassian.sal.spring.connection.SpringHostConnectionAccessor.ConnectionProvider;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSpringHostConnectionAccessor {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private SpringHostConnectionAccessor springHostConnectionAccessor;

    @Mock
    private ConnectionProvider connectionProvider;

    @Mock
    private Connection connection;

    @Mock
    private PlatformTransactionManager transactionManager;

    @Mock
    private ConnectionCallback<Object> callback;

    @Mock
    private Object result;

    @Before
    public void before() {
        springHostConnectionAccessor = new SpringHostConnectionAccessor(connectionProvider, transactionManager);
    }

    @Test
    public void getSchemaName() {
        when(connectionProvider.getSchemaName()).thenReturn(Option.option("schema"));

        assertThat(springHostConnectionAccessor.getSchemaName().get(), is("schema"));
    }

    @Test
    public void execute() {
        when(connectionProvider.getConnection()).thenReturn(connection);
        when(callback.execute(connection)).thenReturn(result);

        assertThat(springHostConnectionAccessor.execute(true, true, callback), is(result));

        verify(callback).execute(connection);
        verifyNoMoreInteractions(callback);
        verifyNoMoreInteractions(connection);
    }
}
