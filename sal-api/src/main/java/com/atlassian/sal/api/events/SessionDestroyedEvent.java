package com.atlassian.sal.api.events;

import javax.annotation.concurrent.Immutable;
import javax.servlet.http.HttpSessionListener;

/**
 * Represents an event published when a http session is destroyed.  Implementers should fire this event accordingly.
 *
 * @see HttpSessionListener
 */
@Immutable
public class SessionDestroyedEvent extends AbstractSessionEvent {
    private SessionDestroyedEvent(final String sessionId, final String userName) {
        super(sessionId, userName);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends AbstractSessionEvent.Builder {
        private Builder() {}

        public SessionDestroyedEvent build() {
            return new SessionDestroyedEvent(sessionId, userName);
        }
    }
}
