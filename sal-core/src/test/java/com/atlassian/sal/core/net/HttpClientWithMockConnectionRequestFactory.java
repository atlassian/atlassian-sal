package com.atlassian.sal.core.net;

import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.protocol.HttpRequestExecutor;

/**
 * This class enables testing our code end-to-end, without HttpClient needing a server to test against
 */
public class HttpClientWithMockConnectionRequestFactory extends HttpClientRequestFactory {

    private final HttpClientConnectionManager mockConnectionManager;
    private final HttpRequestExecutor mockRequestExecutor;

    public HttpClientWithMockConnectionRequestFactory(
            final HttpClientConnectionManager mockConnectionManager, final HttpRequestExecutor mockRequestExecutor) {
        this.mockConnectionManager = mockConnectionManager;
        this.mockRequestExecutor = mockRequestExecutor;
    }

    protected HttpRequestExecutor getRequestExecutor() {
        return mockRequestExecutor;
    }

    /**
     * This mocks out the connection, so we don't actually need a running server
     *
     * @return
     */
    @Override
    protected HttpClientConnectionManager getConnectionManager() {
        return mockConnectionManager;
    }
}
