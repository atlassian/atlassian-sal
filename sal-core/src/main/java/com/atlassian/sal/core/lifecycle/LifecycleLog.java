package com.atlassian.sal.core.lifecycle;

import java.util.Collection;
import java.util.stream.Collectors;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;

import static com.atlassian.plugin.osgi.util.OsgiHeaderUtil.getPluginKey;

/**
 * A collection of methods to make lifecycle logging easier
 */
class LifecycleLog {
    @Nonnull
    static String getPluginKeyFromBundle(@Nullable final Bundle bundle) {
        return (bundle == null) ? "<stale service reference>" : getPluginKey(bundle);
    }

    @Nonnull
    static <T> String listPluginKeys(@Nonnull Collection<ServiceReference<T>> services) {
        Iterable<String> pluginKeys = services.stream()
                .map(service -> getPluginKeyFromBundle(service.getBundle()))
                .collect(Collectors.toList());

        return String.format("[%s]", String.join(", ", pluginKeys));
    }
}
