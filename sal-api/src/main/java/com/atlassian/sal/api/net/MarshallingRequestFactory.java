package com.atlassian.sal.api.net;

/**
 * The REST plugin provides an implementation that allows marshalling of entities.
 *
 * @since 5.1.3
 */
public interface MarshallingRequestFactory<T extends Request<?, ?>> extends RequestFactory<T> {}
