package com.atlassian.sal.testresources.usersettings;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.sal.core.usersettings.DefaultUserSettings;

public class MockUserSettingsService implements UserSettingsService {
    private final Map<UserKey, UserSettings> settingsMap = new HashMap<>();

    @Override
    public UserSettings getUserSettings(UserKey user) {
        return settingsMap.containsKey(user)
                ? settingsMap.get(user)
                : settingsMap.put(user, DefaultUserSettings.builder().build());
    }

    @Override
    public void updateUserSettings(UserKey user, Function<UserSettingsBuilder, UserSettings> updateFunction) {
        if (settingsMap.containsKey(user)) {
            settingsMap.put(user, updateFunction.apply(DefaultUserSettings.builder(settingsMap.get(user))));
        } else {
            settingsMap.put(user, updateFunction.apply(DefaultUserSettings.builder()));
        }
    }
}
