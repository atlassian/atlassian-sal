package com.atlassian.sal.core.component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

import org.junit.Test;

import com.atlassian.sal.api.component.ComponentLocator;

import static org.junit.Assert.assertEquals;

public class TestComponentLocator {

    @Test
    public void testConvertClassToName() {
        MockComponentLocator loc = new MockComponentLocator();
        assertEquals("string", loc.convertClassToName(String.class));
        assertEquals("hashMap", loc.convertClassToName(HashMap.class));
    }

    private static class MockComponentLocator extends ComponentLocator {

        protected <T> T getComponentInternal(Class<T> iface) {
            return null;
        }

        @Override
        protected <T> Collection<T> getComponentsInternal(Class<T> iface) {
            return null;
        }

        @Override
        protected <T> T getComponentInternal(Class<T> iface, String componentId) {
            return null;
        }

        @Override
        protected String convertClassToName(Class cls) {
            return super.convertClassToName(cls);
        }
    }

    public void testGetComponentSafelyNotInitialised() {
        assertEquals(ComponentLocator.getComponentSafely(String.class), Optional.empty());
    }

    public void testGetComponentSafelyEmptyContainer() {
        ComponentLocator.setComponentLocator(new MockComponentLocator());
        assertEquals(ComponentLocator.getComponentSafely(String.class), Optional.empty());
    }
}
