package com.atlassian.sal.core.executor;

import org.junit.Test;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;

import static org.junit.Assert.assertNotNull;

public class TestThreadLocalDelegateRunnable {

    @Test
    public void testRun() throws InterruptedException {
        final ThreadLocalContextManager<Object> manager = new StubThreadLocalContextManager();
        Runnable delegate = new Runnable() {
            public void run() {
                assertNotNull(manager.getThreadLocalContext());
            }
        };

        manager.setThreadLocalContext(new Object());
        Thread t = new Thread(new ThreadLocalDelegateRunnable<Object>(manager, delegate));
        t.start();
        t.join(10000);
        assertNotNull(manager.getThreadLocalContext());
    }
}
