package com.atlassian.sal.testresources.net;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;

/**
 * Mock request that provides getters to all the information that is passed in, and also setters for the
 * response body that should be returned for execute(), or the response that should be passed to the
 * response handler.
 */
public class MockRequest implements Request<MockRequest, MockResponse> {
    private final Request.MethodType methodType;
    private String url;
    private int connectionTimeout;
    private int soTimeout;
    private String requestBody;
    private String requestContentType;
    private final Map<String, List<String>> headers = new HashMap<>();
    private final List<String> requestParameters = new ArrayList<>();
    private String basicHost;
    private String basicUser;
    private String basicPassword;
    private MockResponse response;
    private String responseBody;
    private boolean followRedirects = true;

    public MockRequest(final MethodType methodType, final String url) {
        this.methodType = methodType;
        this.url = url;
    }

    public MockRequest setConnectionTimeout(final int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
        return this;
    }

    public MockRequest setSoTimeout(final int soTimeout) {
        this.soTimeout = soTimeout;
        return this;
    }

    public MockRequest setUrl(final String url) {
        this.url = url;
        return this;
    }

    public MockRequest setRequestBody(final String requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    @Override
    public MockRequest setRequestBody(final String requestBody, final String contentType) {
        this.requestBody = requestBody;
        this.requestContentType = contentType;
        return this;
    }

    public MockRequest setFiles(final List<RequestFilePart> files) {
        return null;
    }

    public MockRequest setEntity(Object entity) {
        throw new UnsupportedOperationException();
    }

    public MockRequest setRequestContentType(final String contentType) {
        this.requestContentType = contentType;
        return this;
    }

    public MockRequest addRequestParameters(final String... params) {
        requestParameters.addAll(Arrays.asList(params));
        return this;
    }

    public MockRequest addHeader(final String headerName, final String headerValue) {
        List<String> list = headers.get(headerName);
        if (list == null) {
            list = new ArrayList<>();
            headers.put(headerName, list);
        }
        list.add(headerValue);
        return this;
    }

    public MockRequest setHeader(final String headerName, final String headerValue) {
        headers.put(headerName, new ArrayList<>(Collections.singletonList(headerValue)));
        return this;
    }

    public MockRequest setFollowRedirects(boolean follow) {
        this.followRedirects = follow;
        return this;
    }

    public MockRequest addBasicAuthentication(final String host, final String username, final String password) {
        basicHost = host;
        basicUser = username;
        basicPassword = password;
        return this;
    }

    public <RET> RET executeAndReturn(ReturningResponseHandler<? super MockResponse, RET> responseHandler)
            throws ResponseException {
        if (response == null) {
            response = new MockResponse();
        }
        return responseHandler.handle(response);
    }

    public void execute(final ResponseHandler<? super MockResponse> responseHandler) throws ResponseException {
        if (response == null) {
            response = new MockResponse();
        }
        responseHandler.handle(response);
    }

    public String execute() throws ResponseException {
        return responseBody;
    }

    public MethodType getMethodType() {
        return methodType;
    }

    public String getUrl() {
        return url;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public String getRequestContentType() {
        return requestContentType;
    }

    public List<String> getRequestParameters() {
        return requestParameters;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public List<String> getHeader(final String headerName) {
        return headers.get(headerName);
    }

    public String getBasicHost() {
        return basicHost;
    }

    public String getBasicUser() {
        return basicUser;
    }

    public String getBasicPassword() {
        return basicPassword;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(final MockResponse response) {
        this.response = response;
    }

    public void setResponseBody(final String responseBody) {
        this.responseBody = responseBody;
    }
}
