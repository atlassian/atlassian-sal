# Atlassian Shared Access Layer (SAL) Plugin

## Description

The Shared Access Layer, or SAL for short, provides a consistent, cohesive API to common plugin
tasks, regardless of the Atlassian application into which your plugin is deployed. SAL is most
useful for cross-application plugin development. If you are developing your plugin for a single
application only, you can simply use the application's own API. If your plugin will run in two
applications or more, you will find SAL's services useful. These common services include, but are
not limited to:

* Job scheduling
* Internationalisation lookups
* Persistence for plugin settings
* A plugin upgrade framework

SAL is a component of the Atlassian plugin development platform. Check our
[SAL availability guide](https://developer.atlassian.com/display/DOCS/SAL+Version+Matrix), then take
a look at the [SAL services](https://developer.atlassian.com/display/DOCS/SAL+Services).

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/SAL).

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/SAL).

### Documentation

[SAL Documentation](https://developer.atlassian.com/display/DOCS/Shared+Access+Layer)


## Code Quality

This repository enforces the Palantir code style using the Spotless Maven plugin. Any violations of the code style will result in a build failure.

### Guidelines:
- **Formatting Code:** Before raising a pull request, run `mvn spotless:apply` to format your code.
- **Configuring IntelliJ:** Follow [this detailed guide](https://hello.atlassian.net/wiki/spaces/AB/pages/4249202122/Spotless+configuration+in+IntelliJ+IDE) to integrate Spotless into your IntelliJ IDE.

