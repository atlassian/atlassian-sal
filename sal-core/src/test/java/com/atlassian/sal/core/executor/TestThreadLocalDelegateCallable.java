package com.atlassian.sal.core.executor;

import java.util.concurrent.Callable;

import org.junit.Test;

import com.atlassian.sal.api.executor.ThreadLocalContextManager;

import static org.junit.Assert.assertNotNull;

public class TestThreadLocalDelegateCallable {

    @Test
    public void testRun() throws InterruptedException {
        final ThreadLocalContextManager<Object> manager = new StubThreadLocalContextManager();
        Callable<Void> delegate = () -> {
            assertNotNull(manager.getThreadLocalContext());
            return null;
        };

        manager.setThreadLocalContext(new Object());
        final Callable<Void> callable = new ThreadLocalDelegateCallable<>(manager, delegate);
        Thread t = new Thread(() -> {
            try {
                callable.call();
            } catch (RuntimeException | Error e) {
                throw e;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        t.start();
        t.join(10000);
        assertNotNull(manager.getThreadLocalContext());
    }
}
