package com.atlassian.sal.core.permission;

import com.atlassian.sal.api.permission.AuthorisationException;
import com.atlassian.sal.api.permission.NotAuthenticatedException;
import com.atlassian.sal.api.permission.PermissionEnforcer;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

public class DefaultPermissionEnforcer implements PermissionEnforcer {

    private final UserManager userManager;

    public DefaultPermissionEnforcer(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public void enforceAdmin() {
        if (!userManager.isAdmin(getRemoteUserOrThrow())) {
            throwNotAuthorised("You must be an administrator to access this resource");
        }
    }

    @Override
    public void enforceAuthenticated() {
        getRemoteUserOrThrow();
    }

    @Override
    public void enforceSiteAccess() {
        UserKey key = userManager.getRemoteUserKey();
        if (key == null) {
            if (!userManager.isAnonymousAccessEnabled()) {
                throwNotAuthenticated("You must be authenticated to access this resource");
            }
        } else if (!(userManager.isLicensed(key) || userManager.isLimitedUnlicensedUser(key))) {
            throwNotAuthorised("You must have at least limited site access to access this resource");
        }
    }

    @Override
    public void enforceSystemAdmin() {
        if (!userManager.isSystemAdmin(getRemoteUserOrThrow())) {
            throwNotAuthorised("You must be a system administrator to access this resource");
        }
    }

    @Override
    public boolean isAdmin() {
        UserKey key = userManager.getRemoteUserKey();
        return key != null && userManager.isAdmin(key);
    }

    @Override
    public boolean isAuthenticated() {
        return userManager.getRemoteUserKey() != null;
    }

    @Override
    public boolean isLicensedOrLimitedUnlicensedUser() {
        UserKey key = userManager.getRemoteUserKey();
        return key != null && (userManager.isLicensed(key) || userManager.isLimitedUnlicensedUser(key));
    }

    @Override
    public boolean isLicensed() {
        UserKey key = userManager.getRemoteUserKey();
        return key != null && userManager.isLicensed(key);
    }

    @Override
    public boolean isSystemAdmin() {
        UserKey key = userManager.getRemoteUserKey();
        return key != null && userManager.isSystemAdmin(key);
    }

    protected void throwNotAuthorised(String message) {
        if (message != null) {
            throw new AuthorisationException(message);
        } else {
            throw new AuthorisationException();
        }
    }

    protected void throwNotAuthenticated(String message) {
        if (message != null) {
            throw new NotAuthenticatedException(message);
        } else {
            throw new NotAuthenticatedException();
        }
    }

    protected UserKey getRemoteUserOrThrow() {
        UserKey key = userManager.getRemoteUserKey();
        if (key == null) {
            throwNotAuthenticated(null);
        }
        return key;
    }
}
