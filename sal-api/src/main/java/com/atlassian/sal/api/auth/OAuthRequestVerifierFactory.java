package com.atlassian.sal.api.auth;

import javax.servlet.ServletRequest;

public interface OAuthRequestVerifierFactory {
    /**
     * Gets an instance of an {@link OAuthRequestVerifier} object that records whether or not the request that is currently
     * being processed has been successfully processed by the OAuth filter.
     *
     * The default implementation of this in SAL ignores the ServletRequest that is passed in and stores and retrieves
     * the value from a thread local.
     */
    OAuthRequestVerifier getInstance(ServletRequest request);
}
