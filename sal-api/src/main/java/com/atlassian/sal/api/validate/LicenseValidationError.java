package com.atlassian.sal.api.validate;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * The license validation error details.
 *
 * @since 4.2
 */
public class LicenseValidationError {

    private final LicenseErrorCode licenseErrorCode;

    private final String errorMessage;

    public LicenseValidationError(
            @Nonnull final LicenseErrorCode licenseErrorCode, @Nonnull final String errorMessage) {
        this.licenseErrorCode = requireNonNull(licenseErrorCode);
        this.errorMessage = requireNonNull(errorMessage);
    }

    /**
     * @return The error type.
     */
    @Nonnull
    public LicenseErrorCode getLicenseErrorCode() {
        return licenseErrorCode;
    }

    /**
     * These should normally be localised to the end user's locale.
     *
     * @return Warning message to explain what is wrong.
     */
    @Nonnull
    public String getErrorMessage() {
        return errorMessage;
    }
}
