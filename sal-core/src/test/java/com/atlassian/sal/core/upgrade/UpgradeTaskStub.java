package com.atlassian.sal.core.upgrade;

import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;

import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;

public class UpgradeTaskStub implements PluginUpgradeTask {

    private int buildNumber;
    private List<Message> errors;
    private boolean upgraded;

    public UpgradeTaskStub(int buildNumber) {
        this.buildNumber = buildNumber;
    }

    public UpgradeTaskStub(int buildNumber, List<Message> errors) {
        this.buildNumber = buildNumber;
        this.errors = errors;
    }

    public int getBuildNumber() {
        return buildNumber;
    }

    @Nonnull
    public String getShortDescription() {
        return (errors == null || errors.isEmpty() ? "Successful" : "Failing") + " stub task for build " + buildNumber;
    }

    public Collection<Message> doUpgrade() {
        upgraded = true;
        return errors;
    }

    public void setErrors(List<Message> errors) {
        this.errors = errors;
    }

    public void setBuildNumber(int buildNumber) {
        this.buildNumber = buildNumber;
    }

    public boolean isUpgraded() {
        return upgraded;
    }

    @Nonnull
    public String getPluginKey() {
        return "com.atlassian.sal.test-stub";
    }
}
