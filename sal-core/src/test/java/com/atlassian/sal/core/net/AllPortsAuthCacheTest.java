package com.atlassian.sal.core.net;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.ChallengeState;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.auth.DigestScheme;
import org.apache.http.message.BasicHeader;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

public class AllPortsAuthCacheTest {
    private static AllPortsAuthCache authCache = new AllPortsAuthCache();
    private static final String hostName = "sample.host";
    private static final HttpHost specificHost = new HttpHost(hostName, 80);
    private static final AuthScheme specificAuthScheme = new BasicScheme();
    private static final AuthScheme allPortsAuthScheme = new DigestScheme();

    @BeforeClass
    public static void before() {
        authCache.put(specificHost, specificAuthScheme);
        authCache.put(new HttpHost(hostName), allPortsAuthScheme);
    }

    @Test
    public void assertSpecificFoundFirst() {
        // BasicCache does serialisation on put and deserialisation on get, we can't check if it's the same object
        assertThat(authCache.get(specificHost), is(instanceOf(BasicScheme.class)));
    }

    @Test
    public void assertAllPortsFound() {
        assertThat(authCache.get(new HttpHost(hostName, 8090)), is(instanceOf(DigestScheme.class)));
    }

    @Test
    public void assertOtherHostNotFound() {
        assertThat(authCache.get(new HttpHost("other.host", 80)), is(nullValue()));
    }

    @Test
    public void assertChallengeStateSurvivesSerialisation() throws Exception {
        AllPortsAuthCache localCache = new AllPortsAuthCache();

        BasicScheme proxyScheme = new BasicScheme();
        proxyScheme.processChallenge(new BasicHeader(HttpHeaders.PROXY_AUTHENTICATE, "Basic "));

        localCache.put(specificHost, proxyScheme);

        BasicScheme outOfCache = (BasicScheme) localCache.get(specificHost);
        assertThat(outOfCache.getChallengeState(), is(ChallengeState.PROXY));
    }
}
