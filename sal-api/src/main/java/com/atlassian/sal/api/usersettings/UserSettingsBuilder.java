package com.atlassian.sal.api.usersettings;

import java.util.Set;

import io.atlassian.fugue.Option;

/**
 * A builder for {@link UserSettings}
 */
public interface UserSettingsBuilder {
    /**
     * add an extra entry to the builder, overwriting any existing value stored against key (regardless of type)
     *
     * @param key   the key to store the value against
     * @param value the non-null String to store, length cannot be longer than {@link UserSettingsService#MAX_KEY_LENGTH}
     * @return this builder
     * @throws IllegalArgumentException if value is null, or value is longer than {@link UserSettingsService#MAX_STRING_VALUE_LENGTH} characters, or key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    UserSettingsBuilder put(String key, String value);

    /**
     * add an extra entry to the builder, overwriting any existing value stored against key (regardless of type)
     *
     * @param key   the key to store the value against
     * @param value the boolean to store
     * @return this builder
     * @throws IllegalArgumentException if key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    UserSettingsBuilder put(String key, boolean value);

    /**
     * add an extra entry to the builder, overwriting any existing value stored against key (regardless of type)
     *
     * @param key   the key to store the value against
     * @param value the long to store
     * @return this builder
     * @throws IllegalArgumentException if key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    UserSettingsBuilder put(String key, long value);

    /**
     * remove an entry from the builder
     *
     * @param key the key for the entry to remove
     * @return this builder
     * @throws IllegalArgumentException if key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    UserSettingsBuilder remove(String key);

    /**
     * @param key the setting key being queried
     * @return a {@link Option.Some Some} containing the value stored against key if one exists, a {@link com.atlassian.fugue.Option#none()}
     * otherwise. Values can be of type String, Boolean or Long.
     * @throws IllegalArgumentException if key is null or longer than {@link UserSettingsService#MAX_KEY_LENGTH} characters.
     */
    Option<Object> get(String key);

    /**
     * @return the set of keys known to this UserSettings
     */
    Set<String> getKeys();

    /**
     * @return an immutable UserSettings matching the contents of this builder
     */
    UserSettings build();
}
